package concurrency;

/**
 * This performs an operation on a group of items of type <tt>T</tt> that are given to it by a {@link Connector}.<br>
 * This, along with {@link Connector}, provides a method for performing asynchronous tasks in operations on large sets of
 * data.
 * 
 * @author Joshua Lipstone
 * @param <T>
 *            the type of item on which the {@link Operator} will be operating
 * @see Connector
 * @see OperatorConstructor
 */
public abstract class Operator<T> extends Thread {
	protected final Object[] structure;
	protected final int start, end, capacity, maxGens;
	protected int size, generation;
	private final Connector<T> source, destination;
	private boolean preFilled, printGenerationData;
	
	public Operator(Object[] structure, int start, int end, int maxGens, Connector<T> source, Connector<T> destination, boolean preFilled) {
		this.structure = structure;
		this.start = start;
		this.end = end;
		this.capacity = end - start;
		this.maxGens = maxGens;
		this.source = source;
		this.destination = destination;
		this.preFilled = preFilled;
		this.printGenerationData = true;
	}
	
	private final synchronized void processLoop() {
		try {
			//This loop requests (end - start) items from the Connector
			//Then waits until the request is filled
			//Once full, it runs process()
			//After the process is done, it prints generation data, increments the generation number, and resets the size field
			while (generation < maxGens) {
				if (!preFilled) {
					request();
					try {
						wait();
					}
					catch (InterruptedException e) {}
					preFilled = false;
				}
				process();
				reset();
			}
			done();
		}
		catch (Throwable e) {
			System.err.println("gens: " + generation + "@" + System.currentTimeMillis());
			e.printStackTrace();
			done();
		}
	}
	
	/**
	 * Adds <tt>item</tt> to this {@link Operator} and notifies the processLoop if it is full.
	 * 
	 * @param item
	 *            the item to add
	 */
	final synchronized void accept(T item) {
		structure[start + size++] = item;
		if (size == capacity)
			notify();
	}
	
	/**
	 * Used by subclasses to pass <tt>item</tt> to the destination {@link Connector}
	 * 
	 * @param item
	 *            the item to pass
	 */
	protected void supply(T item) {
		destination.provide(item);
	}
	
	/**
	 * Requests (end - start) items from the {@link Connector} managing this {@link Operator}
	 */
	private final void request() {
		source.request(this, capacity);
	}
	
	/**
	 * Notifies the {@link Connector} managing this {@link Operator} that this {@link Operator} has completed all of its
	 * generations.
	 */
	private final void done() {
		source.alert(this, "complete");
	}
	
	/**
	 * Prints the generation data if appropriate.<br>
	 * Then increments the {@link #generation} counter.<br>
	 * And then sets {@link #size} to 0.
	 */
	protected void reset() {
		if (printGenerationData && (generation % 10 == 0))
			System.out.println(generationDataToString());
		generation++;
		size = 0;
	}
	
	/**
	 * Subclasses override this method to implement the processing functionality of the {@link Operator}
	 */
	protected abstract void process();
	
	/**
	 * Get an element from the structure array backing the {@link Operator}.<br>
	 * <tt>index</tt> is handled in the structure array as a whole, so the {@link Operator} should only pass values between
	 * {@link #start} and {@link #end}.
	 * 
	 * @param index
	 *            an integer value between {@link #start} inclusive and {@link #end} exclusive
	 * @return the value at <tt>index</tt> in the structure array
	 */
	@SuppressWarnings("unchecked")
	protected T get(int index) {
		return (T) structure[index];
	}
	
	/**
	 * Sets an element in the structure array backing the {@link Operator}.<br>
	 * <tt>index</tt> is handled in the structure array as a whole, so the {@link Operator} should only pass values between
	 * {@link #start} and {@link #end}.
	 * 
	 * @param index
	 *            an integer value between {@link #start} inclusive and {@link #end} exclusive
	 * @param element
	 *            the new value to be placed at <tt>index</tt>
	 * @return the value previously at <tt>index</tt> in the structure array
	 */
	@SuppressWarnings("unchecked")
	protected T set(int index, T element) {
		T old = (T) structure[index];
		structure[index] = element;
		return old;
	}
	
	@Override
	public void run() {
		processLoop();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof Operator))
			return false;
		Operator<?> op = (Operator<?>) o;
		return start == op.start && end == op.end;
	}
	
	/**
	 * Subclasses should override this to provide a more detailed printout, which is ideally still brief.
	 * 
	 * @return information about the current generation
	 */
	public String generationDataToString() {
		return "gen: " + generation + ": (" + start + ", " + end + ")";
	}
	
	/**
	 * @return whether this {@link Operator} is printing information about every 10th generation
	 */
	public boolean isPrintingGenerationData() {
		return printGenerationData;
	}
	
	/**
	 * @param printGenerationData
	 *            whether this {@link Operator} should print information about every 10th generation
	 */
	public void setPrintGenerationData(boolean printGenerationData) {
		this.printGenerationData = printGenerationData;
	}
}
