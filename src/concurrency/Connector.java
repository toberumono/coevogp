package concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Provides a connection between two sets of {@link Operator Operators} and manages the set to which it sends items.<br>
 * This, along with {@link Operator}, provides a method for performing asynchronous tasks in operations on large sets of
 * data.
 * 
 * @author Joshua Lipstone
 * @param <T>
 *            the type of item that the {@link Connector} will be transmitting
 * @see Operator
 * @see OperatorConstructor
 */
public class Connector<T> extends Thread {
	private final Queue<T> cache; //Used to cache items that cannot be passed to Operators at the time that they are submitted
	private final List<Operator<T>> available, working;
	private final List<Integer> requests; //Stores the number of items remaining in requests submitted by individual Operators
	private final Lock lock;
	private boolean initialized;
	
	/**
	 * Constructs a new {@link Connector}, but does <i>not</i> initialize it.
	 */
	public Connector() {
		cache = new SynchronousQueue<>();
		available = Collections.synchronizedList(new ArrayList<>());
		working = Collections.synchronizedList(new ArrayList<>());
		requests = Collections.synchronizedList(new ArrayList<>());
		initialized = false;
		lock = new ReentrantLock();
	}
	
	/**
	 * Initializes this {@link Connector} by creating {@link Operator Operators} using <tt>creator</tt> and binds them to
	 * <tt>structure</tt> with the given <tt>sectionWidth</tt>
	 * 
	 * @param creator
	 *            a function that returns {@link Operator Operators} that work on <tt>structure</tt> and have the given
	 *            <tt>sectionWidth</tt>
	 * @param structure
	 *            an array that already contains all of the items to be worked on if <tt>preFilled</tt> is true, otherwise,
	 *            it's contents do not matter.
	 * @param sectionWidth
	 *            the width of the sections that each {@link Operator} should work on.
	 * @param preFilled
	 *            whether the array already holds all of the items to be processed. This <i>must</i> be set to true for at
	 *            least one group of {@link Operator Operators}.
	 */
	public synchronized void initialize(OperatorConstructor<T> creator, Object[] structure, int sectionWidth, boolean preFilled) {
		if (initialized)
			throw new UnsupportedOperationException("Cannot call initialize on a Connector more than once.");
		initialized = true;
		int s = 0;
		for (int e = sectionWidth; e <= structure.length; s += sectionWidth, e += sectionWidth)
			working.add(creator.construct(structure, s, e, this, preFilled));
		if (structure.length % sectionWidth != 0) //In the event that the structure array is not evenly divided by sectionWidth
			working.add(creator.construct(structure, s, structure.length, this, preFilled));
	}
	
	/**
	 * Called by {@link Operator Operators} being managed by this {@link Connector} to pass a more detailed status update (at
	 * this time "complete" is the only one).
	 * 
	 * @param op
	 *            the {@link Operator} alerting this {@link Connector}
	 * @param status
	 *            the status update
	 */
	public synchronized void alert(Operator<T> op, String status) {
		switch (status) {
			case "complete":
				if (available.contains(op)) {
					available.remove(op);
					requests.remove(op);
				}
				else
					working.remove(op);
				notify();
				return;
			default:
				return;
		}
	}
	
	/**
	 * Called by {@link Operator Operators} being managed by this {@link Connector} to request more elements to process.
	 * 
	 * @param op
	 *            the {@link Operator} alerting this {@link Connector}
	 * @param quantity
	 *            the amount of items being requested
	 */
	public synchronized void request(Operator<T> op, int quantity) {
		try {
			lock.lock();
			if (available.contains(op)) { //If this Operator is already in the request queue, increase the amount it is requesting
				int index = available.indexOf(op);
				requests.set(index, requests.get(index) + quantity);
			}
			else { //Switch the Operator from working to available
				working.remove(op);
				requests.add(quantity);
				available.add(op);
			}
			notify();
		}
		finally {
			lock.unlock();
		}
	}
	
	/**
	 * Called by {@link Operator Operators} not being managed by this {@link Connector} to provide items for the
	 * {@link Operator Operators} being managed by this {@link Connector}.
	 * 
	 * @param item
	 *            the item to provide
	 */
	public synchronized void provide(T item) {
		try {
			lock.lock();
			if (available.size() < 1)
				cache.offer(item);
			else
				flushItem(item);
			notify();
		}
		finally {
			lock.unlock();
		}
	}
	
	/**
	 * Pushes an item to a randomly selected available {@link Operator}.<br>
	 * Logically, this can be thought of as a flushCache-esque operation that only runs for one item at a time.
	 * 
	 * @param item
	 *            the item to flush to an {@link Operator}
	 */
	private void flushItem(T item) {
		try {
			lock.lock();
			int index = ThreadLocalRandom.current().nextInt(available.size());
			available.get(index).accept(item);
			int remainder = requests.get(index) - 1;
			if (remainder <= 0) { //If this was the last item requested, move the Operator from the available pool into the working pool
				Operator<T> op = available.remove(index);
				requests.remove(index);
				working.add(op);
			}
			else
				requests.set(index, remainder);
		}
		finally {
			lock.unlock();
		}
	}
	
	@Override
	public void run() {
		if (!initialized)
			throw new UnsupportedOperationException("Cannot start a Connector that has not been initialized.");
		try {
			lock.lock();
			for (Operator<T> op : working)
				op.start();
		}
		finally {
			lock.unlock();
		}
		synchronized (this) {
			try {
				while (working.size() + available.size() != 0) { //While there are Operators managed by this Connector
					try {
						lock.lock();
						//While there are items in the cache and there are unfilled requests, flush items from the cache into Operators
						while (cache.size() > 0 && available.size() > 0)
							flushItem(cache.poll());
					}
					finally {
						lock.unlock();
					}
					wait(100); //This ensures that the connector will monitor connections while the Operators are starting.
				}
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void forceStop() {
		while (working.size() > 0)
			working.remove(0).interrupt();
		while (available.size() > 0)
			available.remove(0).interrupt();
	}
}
