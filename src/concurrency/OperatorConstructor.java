package concurrency;

/**
 * Represents a function that produces an {@link Operator}.<br>
 * This is used in {@link Connector} to allow for currying of the {@link Operator} constructor.
 * 
 * @author Joshua Lipstone
 * @param <T>
 *            the type of object that the {@link Operator Operators} produced by this method will be working with
 * @see Connector
 * @see Operator
 */
@FunctionalInterface
public interface OperatorConstructor<T> {
	public Operator<T> construct(Object[] structure, int start, int end, Connector<T> source, boolean preFilled);
}
