// gpjpp (genetic programming package for Java)
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of version 2 of the GNU General Public
// License as published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

package coevogp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import coevogp.config.Configuration;
import coevogp.config.Properties;
import coevogp.gp.Drawing;
import coevogp.gp.GP;
import coevogp.gp.NodeSet;
import coevogp.gp.Population;

/**
 * Abstract class that encapsulates the details of running a genetic programming test in a Java console application.
 * <p>
 * The user must override two methods of this class: createNodeSet(), which defines the branch structure and the node types
 * used in each branch of each tree; and createPopulation(), which creates an instance of a user-defined subclass of
 * GPPopulation.
 * <p>
 * It is common to override another method of this class: createVariables(), which creates an instance of Configuration with
 * the default configuration parameters for the run. Frequently you may need to add problem-specific configuration variables
 * (the size of an ant trail, e.g.), which is best done by subclassing Configuration and overriding createVariables() to
 * create an instance of your own class.
 * <p>
 * GPRun then handles numerous details of managing a test run. These include:
 * <p>
 * 1. Initializing the random number generator used by gpjpp.
 * <p>
 * 2. Reading a configuration file so that most properties of the test run can be adjusted without recompiling. Also creates
 * a default configuration file if the named one isn't found.
 * <p>
 * 3. Creating output files for run statistics and detailed population reporting.
 * <p>
 * 4. Registering all necessary classes with the stream manager.
 * <p>
 * 5. Trapping all exceptions to report a detailed stack trace in case of error.
 * <p>
 * 6. Loading a previous checkpoint stream if found, or creating an initial population of individuals.
 * <p>
 * 7. Displaying status output to the console window and also printing configurable output to report files.
 * <p>
 * 8. Streaming a checkpoint file after a configurable number of generations in case a long run needs to be interrupted.
 * <p>
 * 9. Running a configurable number of generations and terminating the run when a configurable fitness target is reached.
 * <p>
 * 10.Writing configurable reports on the best individual at the end of a run, including graphic images of its tree structure
 * and possibly its fitness performance (such as the trail of an ant or a lawnmower).
 * <p>
 * 11.Running multiple runs until a configurable number of good runs is found, and timing each run's performance.
 * <p>
 * GPRun is divided into a number of reasonably small routines so that additional aspects of its behavior can be customized
 * by creating a subclass. Nevertheless, it is not suitable for writing applets or other graphics-intensive Java
 * applications. These could be written by using the lower level classes of gpjpp directly, since these classes enforce no
 * user interface of their own.
 * <p>
 *
 * @version 2.0
 * @author Joshua Lipstone
 */
public abstract class GPRun<C extends Configuration, P extends GP<C, R>, Pl extends Player<C>, R> {
	
	/**
	 * An ID code written at the end of checkpoint streams. This is primarily useful for checking to see whether a stream was
	 * fully flushed in the event of a machine crash.
	 */
	protected static final int RUNENDID = 0x87654321;
	
	/**
	 * The size in bytes of the output buffer used for PrintStreams. These streams are flushed at the end of every line, so
	 * there is little point in using a larger buffer.
	 */
	protected static int outBufSize = 128;  //buffer for GPPrintStreams
	
	/**
	 * The size in bytes of the input or output buffer used for a DataInputStream or a DataOutputStream during checkpoint
	 * loading or saving. A reasonably large buffer provides better performance, since these streams are composed of very
	 * many small data items.
	 */
	protected static int stmBufSize = 4096; //buffer for DataIOStreams
	
	/**
	 * The base name for all input and output files used in the run. To get the configuration file name, ".ini" is appended
	 * to baseName. To get the statistics file name, ".stc" is appended to baseName. To get the detailed population file
	 * name, ".det" is appended to baseName. To get the checkpoint file name, ".stm" is appended to baseName. This field is
	 * passed as a parameter to the GPRun constructor.
	 */
	protected String baseName;
	
	/**
	 * The configuration variables used for the run. The instance is created by the createVariables() abstract method. Its
	 * values are assigned from the configuration file (baseName+".ini") if found.
	 */
	protected C cfg;
	
	/**
	 * The branch and node definitions for all the GPs used during the run.
	 */
	protected NodeSet<C, R> ns;
	
	/**
	 * The PrintStream used for the detailed population output file. If cfg.PrintDetails is false, this field is null and the
	 * report file is not created.
	 */
	protected PrintWriter dout;
	
	/**
	 * The PrintStream used for statistics and summary reporting. This file (baseName+".stc") is always created. It includes
	 * a list of the configuration parameters, a list of the branch and node definitions, the printStatistics output for each
	 * generation, timing per run and per generation, and a report on the best individual at the end of each run. In case a
	 * run is restarted from a checkpoint, additional output is appended to the existing statistics file if found.
	 */
	protected PrintWriter sout;
	
	/**
	 * The main population, instantiated in GPRun() and reused repeatedly in run().
	 */
	protected Population<C, Pl, R> pop;
	
	/**
	 * The secondary population. If cfg.SteadyState is true, newPop is not used and remains null. Otherwise each new
	 * generation is created in the run() method by calling pop.generate(newPop). Then the newPop and pop references are
	 * swapped to proceed with the next generation.
	 */
	protected Population<C, Pl, R> newPop;
	
	/**
	 * The current generation number. It starts with 0 for the initial population and increments to cfg.NumberOfGenerations.
	 * When restarted from a checkpoint, the run starts again from the last generation that was checkpointed.
	 */
	protected int curGen;
	
	/**
	 * The current run number.
	 */
	protected int curRun;
	
	/**
	 * The number of good runs found so far. A good run is one where the best individual's fitness becomes less than
	 * cfg.TerminationFitness.
	 */
	protected int goodRuns;
	
	/**
	 * The number of generations until the next checkpoint will be stored. If cfg.CheckpointGens is 0, this field is not
	 * used. Otherwise it is initialized to cfg.CheckpointGens and decremented after each generation.
	 */
	protected int cntGen;
	
	//==== abstract methods ====
	
	/**
	 * Returns a properly initialized instance of GPAdfNodeSet, containing the branch and node definitions for this problem.
	 * You must override this method to create your own set of functions and terminals. Your version can create a different
	 * node set depending on whether cfg.UseADFs is true or false. createNodeSet() can also refer to other user-defined
	 * configuration parameters when cfg is a subclass of Configuration to further customize the node set to the problem
	 * configuration.
	 * 
	 * @param cfg
	 *            the Configuration data
	 * @return a {@link NodeSet} with both functions and terminals
	 */
	abstract protected NodeSet<C, R> createNodeSet(C cfg);
	
	/**
	 * You must override this method to return a new instance of a user subclass of GPPopulation. The user subclass must at
	 * least override the createGP() method of GPPopulation to create instances of a user subclass of GP. If the
	 * configuration does not specify a steady state population, createPopulation() is called twice; otherwise it is called
	 * just once regardless of the number of generations or runs.
	 * 
	 * @param cfg
	 *            the Configuration data
	 * @param ns
	 *            a fully initialized {@link NodeSet}
	 * @return a {@link Population} initialized with the given {@link Configuration} and {@link NodeSet}
	 */
	abstract protected Population<C, Pl, R> createPopulation(C cfg, NodeSet<C, R> ns);
	
	//==== remaining methods are not abstract ====
	
	/**
	 * This constructor allocates the basic data structures and prepares for a run. GPRun() traps all exceptions. If it
	 * catches one, it dumps a stack trace and calls System.exit(1).
	 *
	 * @param baseName
	 *            specifies the base file name for the run's configuration file, output files, and stream file.
	 * @param createDefaultIni
	 *            if true, and if baseName+".ini" is not found, GPRun creates a configuration file holding the default
	 *            configuration values.
	 */
	public GPRun(String baseName, boolean createDefaultIni) {
		
		//trap all exceptions to report details
		try {
			if (baseName.toLowerCase().endsWith(".ini")) //strip .ini from baseName
				this.baseName = baseName.substring(0, baseName.length() - 4);
			else
				this.baseName = baseName;
			
			//create configuration class
			cfg = createConfiguration();
			
			//read configuration parameters
			readIni(this.baseName + ".ini", createDefaultIni);
			
			//create the function and terminal descriptions
			ns = createNodeSet(cfg);
			
			//make population instances; individuals aren't created until run()
			pop = createPopulation(cfg, ns);
			if (!cfg.SteadyState)
				newPop = createPopulation(cfg, ns);
			
			//open output files
			boolean append = (getCheckpointFile() != null);
			sout = createOrAppend(getStatisticsPath(), append);
			if (cfg.PrintDetails)
				dout = createOrAppend(getDetailPath(), append);
			else
				dout = null;
		}
		catch (Throwable e) {
			//provide details on any exceptions thrown and halt
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * The main method of GPRun creates and evolves populations, writes reports, loads and saves checkpoint files, and does
	 * multiple runs until a configurable number of good ones is found. run() traps all exceptions. If it catches one, it
	 * dumps a stack trace and calls System.exit(1). run() calls a number of small methods for reporting output to the
	 * console and to files; these can be overridden to make small changes to its behavior. However, most changes are
	 * accomplished by modifying the configuration file that controls the run.
	 */
	public void run() {
		
		//trap all exceptions to report details
		try {
			curGen = 0;
			curRun = 1;
			goodRuns = 0;
			cntGen = cfg.CheckpointGens;
			
			//load checkpoint if available
			boolean loadedCheckpoint = false; //TODO loadCheckpoint();
			if (!loadedCheckpoint)
				showConfiguration();
			
			//do runs until some that reach terminating fitness are found
			do {
				//display run number
				showRunNumber(curRun, goodRuns);
				
				//time the run
				long timeStart = System.currentTimeMillis();
				
				if (loadedCheckpoint)
					//create population next run through
					loadedCheckpoint = false;
				else {
					//create initial population
					showCreation(true);
					pop.create();
					showCreation(false);
				}
				
				//show initial generation
				showGeneration(true, curGen, false);
				
				//time the generations
				long timeGenStart = System.currentTimeMillis();
				int gens = 0;
				boolean goodRun = false;
				
				//loop through the generations
				//while (curGen < cfg.NumberOfGenerations) {
				//create next generation
				curGen++;
				gens++;
				//if (cfg.SteadyState)
					pop = pop.generate(newPop);
				/*else {
					newPop = pop.generate(newPop);
					//swap the pops
					Population<C, Pl, R> tmp = pop;
					pop = newPop;
					newPop = tmp;
					//ensure no references remain to obsolete GPs
					newPop.clear();
				}*/
				
				//checkpoint this generation's population
				boolean savedCheckpoint = false; //TODO saveCheckpoint();
				
				//show this generation
				showGeneration(false, curGen, savedCheckpoint);
				pop.printStatistics(10, ' ', sout);
				
				goodRun = (pop.bestFitness < cfg.TerminationFitness);
				if (goodRun) {
					//break if terminating fitness found
					goodRuns++;
					break;
				}
				//}
				
				//read time for generation rate
				long timeGenStop = System.currentTimeMillis();
				
				//report on final generation
				showFinalGeneration(curGen, goodRun);
				
				//compute and display run timing
				long timeStop = System.currentTimeMillis();
				double secsTotal = (timeStop - timeStart) / 1000.0;
				double secsPerGen;
				if (gens > 0)
					secsPerGen = (timeGenStop - timeGenStart) / (1000.0 * gens);
				else
					secsPerGen = 0.0;
				showTiming(secsTotal, secsPerGen);
				
				//erase final stream file of run
				if (cfg.CheckpointGens > 0)
					(new File(getCheckpointName())).delete();
				
				//update counters for next run
				curRun++;
				curGen = 0;
				cntGen = cfg.CheckpointGens;
				
			} while (goodRuns < cfg.GoodRuns);
			
			//close output files
			sout.close();
			if (cfg.PrintDetails)
				dout.close();
			
		}
		catch (Throwable e) {
			System.err.println();
			System.err.println();
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * @return the best individual in the current {@link Population}
	 * @see Population#getBestOfPop()
	 */
	public GP<C, R> getBest() {
		return pop.getBestOfPop();
	}
	
	/**
	 * This method must be overridden to return the appropriate implementation of {@link Configuration}
	 * 
	 * @return a valid instance of the type of {@link Configuration} represented by C
	 */
	protected C createConfiguration() {
		@SuppressWarnings("unchecked")
		C cfg = (C) new Configuration();
		return cfg;
	}
	
	/**
	 * Convenience method for {@link #readIni(Path, boolean)}.<br>
	 * Reads the specified file to get run configuration variables. Calls {@link Properties#load(Path)} to interpret the
	 * file. If the file is not found and <tt>createDefaultIni</tt> is true, {@link #readIni(Path, boolean)} creates a new
	 * file and writes the current values of {@link #cfg} to this file using {@link Configuration#printOn(PrintWriter)}
	 * 
	 * @param iniPath
	 *            the path to the .ini file to load
	 * @param createDefaultIni
	 *            whether to create the .ini file if it does not exist.
	 * @throws IOException
	 *             if there is an error reading or creating the .ini file
	 * @see #readIni(Path, boolean)
	 * @see Properties#Properties(Path)
	 * @see Configuration#load(Properties)
	 * @see Configuration#printOn(PrintWriter)
	 */
	protected void readIni(String iniPath, boolean createDefaultIni) throws IOException {
		readIni(Paths.get(iniPath), createDefaultIni);
	}
	
	/**
	 * Reads the specified file to get run configuration variables. Calls {@link Properties#load(Path)} to interpret the
	 * file. If the file is not found and <tt>createDefaultIni</tt> is true, {@link #readIni(Path, boolean)} creates a new
	 * file and writes the current values of {@link #cfg} to this file using {@link Configuration#printOn(PrintWriter)}
	 * 
	 * @param ini
	 *            the path to the .ini file to load
	 * @param createDefaultIni
	 *            whether to create the .ini file if it does not exist.
	 * @throws IOException
	 *             if there is an error reading or creating the .ini file
	 * @see #readIni(String, boolean)
	 * @see Properties#Properties(Path)
	 * @see Configuration#load(Properties)
	 * @see Configuration#printOn(PrintWriter)
	 */
	protected void readIni(Path ini, boolean createDefaultIni) throws IOException {
		if (!ini.toFile().exists() && createDefaultIni)
			//.ini file not found, use defaults
			try {
				//create ini file for next time
				dout = createOrAppend(ini, false);
				cfg.printOn(dout);
				dout.close();
			}
			catch (Exception f) {
				f.printStackTrace();
				throw new Error("Can't create " + ini.toString());
			}
		cfg.load(new Properties(ini));
	}
	
	/**
	 * Creates or appends to a specified file and returns a valid reference to a {@link PrintWriter} for that file.<br>
	 * If the file or the path to that file does not exist, {@link #createOrAppend(Path, boolean)} creates the directories
	 * and files that are missing before opening the file for writing.<br>
	 * Once the file is confirmed to exist, {@link #createOrAppend(Path, boolean)} calls
	 * {@code new PrintWriter(new BufferedWriter(new FileWriter(file, append)))}.
	 * 
	 * @param path
	 *            the path to the file
	 * @param append
	 *            whether to append text to the existing file or overwrite it
	 * @return a {@link PrintWriter} that writes to the file at <tt>path</tt>
	 * @throws IOException
	 *             if there was an error opening the file
	 */
	protected PrintWriter createOrAppend(Path path, boolean append) throws IOException {
		File file = path.toFile();
		file.createNewFile();
		return new PrintWriter(new BufferedWriter(new FileWriter(file, append)));
	}
	
	/**
	 * Prints a {@link String} to {@link System#out} and to {@link #sout}.
	 * 
	 * @param str
	 *            the {@link String} to print
	 */
	protected void echoPrint(String str) {
		System.out.println(str);
		sout.println(str);
	}
	
	/**
	 * Prints the configuration variables and the node definitions to {@link #sout}.
	 */
	protected void showConfiguration() {
		//print configuration parameters
		cfg.printOn(sout);
		sout.println();
		
		//print tree descriptions
		ns.printOn(sout, cfg);
		sout.println();
	}
	
	/**
	 * Prints the current run number and number of good runs so far to System.out, {@link #sout}, and {@link #dout}.
	 * 
	 * @param curRun
	 *            the current run number
	 * @param goodRuns
	 *            the number of good runs
	 */
	protected void showRunNumber(int curRun, int goodRuns) {
		echoPrint("");
		String dispStr = "Run number " + curRun + " (good runs " + goodRuns + ")";
		echoPrint(dispStr);
		if (cfg.PrintDetails)
			dout.println(dispStr);
	}
	
	/**
	 * Prints a status message to {@link System#out} while the initial population is being created. When the population is
	 * done, finishes the status message and writes to {@link System#out} and {@link #sout} the number of individuals that
	 * were rejected because they were too complex or were duplicates.
	 * 
	 * @param preCreation
	 *            if true, it just prints, "Creating initial population... " to {@link System#out System.out.print}
	 */
	protected void showCreation(boolean preCreation) {
		if (preCreation)
			System.out.print("Creating initial population... ");
		else {
			System.out.println("Ok");
			
			echoPrint("Too complex " + pop.attemptedComplexCount);
			if (cfg.TestDiversity)
				echoPrint("Duplicate " + pop.attemptedDupCount);
			sout.println();
		}
	}
	
	/**
	 * Returns the symbols displayed in the statistical display when a checkpoint is finished or skipped. The default
	 * implementation returns {@code 'c'} for a checkpoint and {@code ' '} for no checkpoint.
	 * 
	 * @param savedCheckpoint
	 *            whether a checkpoint was saved
	 * @return {@code 'c'} if savedCheckpoint is {@code true}, otherwise {@code ' '}
	 */
	protected char checkChar(boolean savedCheckpoint) {
		return savedCheckpoint ? 'c' : ' ';
	}
	
	/**
	 * Prints information to {@link System#out} and to {@link sout} about the generation just completed.
	 * {@link Population#printStatisticsLegend(PrintWriter)} is called if showLegend is true.
	 * {@link Population#printStatistics(int, char, PrintWriter)} is always called.<br>
	 * {@link Population#printDetails(int, boolean, boolean, boolean, boolean, boolean, PrintWriter)} is called only if
	 * {@link Configuration#PrintDetails} is true and it prints to {@link #dout}.
	 * 
	 * @param showLegend
	 *            whether to show the legend
	 * @param curGen
	 *            the current generation
	 * @param savedCheckpoint
	 *            whether a checkpoint was saved
	 * @see Population#printStatisticsLegend(PrintWriter)
	 * @see Population#printStatistics(int, char, PrintWriter)
	 * @see Population#printDetails(int, boolean, boolean, boolean, boolean, boolean, PrintWriter)
	 */
	protected void showGeneration(boolean showLegend, int curGen, boolean savedCheckpoint) {
		if (showLegend) {
			pop.printStatisticsLegend(new PrintWriter(System.out));
			pop.printStatisticsLegend(sout);
		}
		
		pop.printStatistics(curGen, checkChar(savedCheckpoint), new PrintWriter(System.out));
		pop.printStatistics(curGen, checkChar(savedCheckpoint), sout);
		
		if (cfg.PrintDetails) {
			//print details about this generation
			pop.printDetails(curGen,
					cfg.PrintPopulation,	//showAll
					true,				   //showBest
					true,				   //showWorst
					cfg.PrintExpression,	//showExpression
					false,				  //showTree
					dout);
		}
	}
	
	/**
	 * Prints information about the final generation of a run. Calls GPPopulation.printDetails to write information about the
	 * run's best individual to {@link #sout} . If {@link Configuration#PrintTree cfg.PrintTree} is true, a {@link Drawing}
	 * surface is created and {@link GP#drawOn(Drawing, String, Configuration)} is called to draw the best individual's trees
	 * to gif files. These files are named as follows: <code>baseName+curRun+branchName+".gif"</code> where branchName is
	 * "RPB" for the result-producing branch and "ADFn" for the ADF branches, or "" for single-branch trees.
	 * <p>
	 * For some Java implementations, the console window loses focus temporarily while the off-screen drawing window is
	 * active. Focus is returned to the previous window once drawing is complete.
	 *
	 * @param curGen
	 *            the final generation number, which can be cfg.NumberOfGenerations or less.
	 * @param goodRun
	 *            true if the best individual's fitness was less than cfg.TerminationFitness.
	 * @throws IOException
	 *             if an I/O error occurs while writing gif files.
	 */
	protected void showFinalGeneration(int curGen, boolean goodRun) throws IOException {
		
		//print details about best individual of run
		sout.println();
		pop.printDetails(curGen,
				false,				  //showAll
				true,				   //showBest
				false,				  //showWorst
				cfg.PrintExpression,	//showExpression
				cfg.PrintTree,		  //showTree
				sout);
		
		if (cfg.PrintTree) {
			System.out.print("Drawing best individual... ");
			
			//get a drawing surface (may grab focus from console window)
			Drawing ods = new Drawing();
			
			//make gif files of the best individual found
			//user can override GP.drawOn to print a trail or
			//  other problem-specific graphic too
			pop.get(pop.bestOfPopulation).drawOn(ods, "data/" + baseName + curRun, cfg);
			
			//return system resources (return focus to console window)
			ods.dispose();
			
			System.out.println("Ok");
		}
	}
	
	/**
	 * Print to {@link System#out} and to {@link #sout} the total elapsed seconds for a run and also the number of seconds to
	 * process each generation. The latter figure does not include time spent creating the initial population or printing
	 * details about the final generation.
	 * 
	 * @param elapsedSecs
	 *            the total elapsed seconds
	 * @param secsPerGen
	 *            the average number of elapsed seconds per generation
	 */
	protected void showTiming(double elapsedSecs, double secsPerGen) {
		echoPrint("Run time " +
				Population.trimFormatDouble(elapsedSecs, 9, 2) + " seconds, " +
				Population.trimFormatDouble(secsPerGen, 9, 2) + " seconds/generation");
		sout.println();
	}
	
	/**
	 * @return the checkpoint file name, {@code "data/" + baseName + ".stm"} by default.
	 * @see #getCheckpointPath()
	 */
	protected String getCheckpointName() {
		return "data/" + baseName + ".stm";
	}
	
	/**
	 * @return a {@link Path} to the checkpoint file.
	 * @see #getCheckpointName()
	 */
	protected Path getCheckpointPath() {
		return Paths.get(getCheckpointName());
	}
	
	/**
	 * @return the statistics file name, {@code "data/" + baseName + ".stc"} by default.
	 * @see #getStatisticsPath()
	 */
	protected String getStatisticsName() {
		return "data/" + baseName + ".stc";
	}
	
	/**
	 * @return a {@link Path} to the statistics file.
	 * @see #getStatisticsName()
	 */
	protected Path getStatisticsPath() {
		return Paths.get(getStatisticsName());
	}
	
	/**
	 * @return the detail file name, {@code "data/" + baseName + ".det"} by default.
	 * @see #getDetailPath()
	 */
	protected String getDetailName() {
		return "data/" + baseName + ".det";
	}
	
	/**
	 * @return a {@link Path} to the details file.
	 * @see #getDetailName()
	 */
	protected Path getDetailPath() {
		return Paths.get(getDetailName());
	}
	
	/**
	 * @return an instantiated {@link File} of the stream file if checkpointing is enabled and the stream file exists.
	 *         Otherwise returns null.
	 */
	protected File getCheckpointFile() {
		if (cfg.CheckpointGens > 0) {
			File f = new File(getCheckpointName());
			if (f.exists())
				return f;
		}
		return null;
	}
}
