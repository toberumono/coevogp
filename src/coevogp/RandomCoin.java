package coevogp;

import java.util.Random;

/**
 * Extends the standard {@link java.util.Random Random} class with the {@link #flip(double)} and {@link #nextInt(int, int)}
 * methods.<br>
 * A static instance of {@link RandomCoin} can be found in {@link coevogp.gp.GP GP}.
 * 
 * @version 2.0
 */
public class RandomCoin extends Random {
	
	/**
	 * Returns true if a random real in the range from 0.0 to 100.0 is less than the specified percent. Thus, if percent is
	 * 50.0, the result has equal probability of returning true or false.
	 * 
	 * @param percent
	 *            the non-decimal form percent chance of returning true
	 * @return the result of a coin flip with the given percentage weights
	 */
	public boolean flip(double percent) {
		return (nextDouble() * 100.0 < percent) ? true : false;
	}
	
	/**
	 * Returns a random number in the range [start, end)
	 * 
	 * @param start
	 *            the lower bound of the range (inclusive)
	 * @param end
	 *            the upper bound of the range (exclusive)
	 * @return a random number in [start, end)
	 */
	public int nextInt(int start, int end) {
		return start + nextInt(end - start);
	}
}
