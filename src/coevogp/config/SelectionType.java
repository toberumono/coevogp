package coevogp.config;

/**
 * The available methods of selection
 * 
 * @author Joshua Lipstone
 */
public enum SelectionType {
	
	/**
	 * A fitness-based selection type that uses a roulette algorithm. The default method.
	 */
	ProbabilisticSelection,
	
	/**
	 * A fitness-based selection type that uses a tournament algorithm.
	 */
	TournamentSelection,
	
	/**
	 * A fitness-based selection type that uses greedy over-selection. When this type is enabled, demetic grouping is always
	 * disabled.
	 */
	GreedySelection
	
}
