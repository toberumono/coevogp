package coevogp.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a field in an implementation of {@link Configuration} that should be copied in the clone constructor and should be
 * loaded from and saved to files.
 * 
 * @author Joshua Lipstone
 * @see Configuration
 * @see Copy
 * @see Clone
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Property {
	/**
	 * Only used for String[]-based enums
	 * 
	 * @return the possible values for the String[]-based enum. This must not be set for any other value type.
	 */
	String[] selection() default {};
}
