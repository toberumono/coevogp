package coevogp.config;

import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Arrays;

import coevogp.GPRun;
import coevogp.gp.GP;
import coevogp.gp.Population;

/**
 * Holds all global configuration parameters for a genetic programming run. All parameters have default values as described
 * below. The {@link GPRun} class also automatically reads a configuration file in Java property or Windows .ini format which
 * can be used to set configuration parameters without recompiling an application.
 * <p>
 * This class uses the reflections API and the {@link Property}, {@link Clone}, and {@link Copy} annotations to dynamically
 * create the copy constructor and the {@link #equals(Object)}, {@link #load(Properties)}, and {@link #printOn(PrintWriter)}
 * methods for all of its subclasses at runtime.
 *
 * @author Joshua Lipstone
 * @version 2.0
 * @see Property
 * @see Clone
 * @see Copy
 */
public class Configuration implements Cloneable {
	
	//variables controlling a run, with defaults
	//===================================================================
	
	/**
	 * The number of individuals (GP instances) in a population. If SteadyState is false, two populations of this size are
	 * created and filled. If SteadyState is true, only one population is created, and weaker individuals are replaced by new
	 * individuals as they are created.<br>
	 * Default: 500
	 */
	@Property public int PopulationSize = 500;
	
	/**
	 * The maximum number of generations in a run. If the best individual's standardized fitness drops below
	 * {@link #TerminationFitness}, the run will terminate sooner.<br>
	 * Default: 150
	 */
	@Property public int NumberOfGenerations = 150;
	
	/**
	 * The strategy used to {@link Population#create create} generation 0.<br>
	 * Default: {@link TreeCreationType#RampedHalf}
	 */
	@Property public TreeCreationType CreationType = TreeCreationType.RampedHalf;
	
	/**
	 * The largest depth allowed for newly created trees.<br>
	 * Default: 6<br>
	 * The minimum tree depth is always 2.
	 */
	@Property public int MaximumDepthForCreation = 6;
	
	/**
	 * The largest depth allowed for trees after {@link GP#cross(GP, int, int) crossover}.<br>
	 * Default: 17<br>
	 * <b>Note:</b> {@link #MaximumDepthForCrossover} must always be at least as large as {@link #MaximumDepthForCreation}.
	 */
	@Property public int MaximumDepthForCrossover = 17;
	
	/**
	 * The largest complexity (number of nodes) allowed in any individual GP, including the main branch and all ADFs.
	 * Although keeping a tight rein on complexity can speed the algorithm and cut memory requirements, be cautious not to
	 * limit the complexity too much. This can lead to lack of diversity in the population and to wasted time attempting to
	 * create and evolve new individuals.<br>
	 * Default: 200
	 *
	 * @see Population#create()
	 */
	@Property public int MaximumComplexity = 200;
	
	/**
	 * The strategy used to select individuals with a probability related to their fitness.<br>
	 * Default: {@link SelectionType#TournamentSelection}
	 */
	@Property public SelectionType SelType = SelectionType.TournamentSelection;
	
	/**
	 * When {@link EvolutionType} is set to {@link EvolutionType#SinglePopulationCompetitiveCoevolution}, this number
	 * determines the size of the blocks that the population is broken up into for parallelization, and must be at least
	 * <tt>2</tt>. Otherwise, this is the number of randomly selected individuals forming a "tournament" when
	 * {@link #SelType} is {@link SelectionType#TournamentSelection}.<br>
	 * Default: 20
	 */
	@Property public int TournamentSize = 20;
	
	/**
	 * Used to determine the type of evolution that will be used.<br>
	 * Default: {@link EvolutionType#SinglePopulationEvolution}
	 */
	@Property public EvolutionType EvoType = EvolutionType.SinglePopulationEvolution;
	
	/**
	 * Percent probability that a crossover operation will occur while evolving a new generation.<br>
	 * Default: 70.0
	 */
	@Property public double CrossoverProbability = 70.0;
	
	/**
	 * Percent probability that a creation operation will occur while evolving a new generation.<br>
	 * Default: 0.0
	 */
	@Property public double CreationProbability = 0.0;
	
	/**
	 * Percent probability that {@link GP#swapMutation swap mutation} will occur for each individual added to a new
	 * generation.<br>
	 * Default: 20.0
	 */
	@Property public double SwapMutationProbability = 20.0;
	
	/**
	 * Percent probability that {@link GP#shrinkMutation shrink mutation} will occur for each individual added to a new
	 * generation.<br>
	 * Default: 20.0
	 */
	@Property public double ShrinkMutationProbability = 20.0;
	
	/**
	 * A run terminates if the best individual in any generation has standardized fitness below this level. Because fitness
	 * must always be non-negative, the default value will never cause a run to stop before {@link #NumberOfGenerations}.<br>
	 * Default: 0.0
	 */
	@Property public double TerminationFitness = 0.0;
	
	/**
	 * {@link GPRun} does multiple runs until this many of them terminate with the best individual's fitness below
	 * TerminationFitness. Set it to 0 if you want just a single run regardless of results.<br>
	 * Default: 0
	 */
	@Property public int GoodRuns = 0;
	
	/**
	 * A boolean that can be tested by user functions to determine whether to add ADFs to the branch definition and whether
	 * to evaluate them in fitness functions. Provides a convenient way to test performance without recompiling. This
	 * configuration variable is not used by the default methods in the library itself.<br>
	 * Default: false
	 */
	@Property public boolean UseADFs = false;
	
	/**
	 * Determines whether gpjpp tests the diversity of genetic populations. If true, gpjpp guarantees that there are no
	 * duplicate individuals in generation 0 and then tracks and reports the diversity of succeeding generations. If false,
	 * gpjpp does not verify or measure diversity.<br>
	 * Default: true
	 */
	@Property public boolean TestDiversity = true;
	
	/**
	 * A boolean that can be tested by the user evaluate() function to determine whether to incorporate complexity into the
	 * fitness calculation. When this is done, solutions tend to be parsimonious and average population complexity tends to
	 * remain lower. Provides a convenient way to test population statistics without recompiling. This configuration variable
	 * is not used by gpjpp itself.<br>
	 * Default: true
	 */
	@Property public boolean ComplexityAffectsFitness = true;
	
	/**
	 * The number of generations between checkpoints saved by {@link GPRun}. If 0, checkpointing is not performed at all, in
	 * which case runs cannot be restarted after a crash or system shutdown. If 1, a checkpoint is saved after every
	 * generation.<br>
	 * Default: 0
	 */
	@Property public int CheckpointGens = 0;
	
	/**
	 * Determines whether demetic migration is performed. If true, the overall population is subdivided into groups of size
	 * {@link #DemeSize}. These groups are isolated for the purpose of fitness-based selection. After all the demetic groups
	 * of a new generation are created, one individual from each group is fitness-selected and swapped with a
	 * fitness-selected individual from the next group, with each swap controlled by the probability
	 * {@link #DemeticMigProbability}.<br>
	 * Default: false
	 */
	@Property public boolean DemeticGrouping = false;
	
	/**
	 * The number of individuals in each demetic group, if {@link #DemeticGrouping} is true. {@link #PopulationSize} must be
	 * an exact integer multiple of DemeSize.<br>
	 * Default: 100
	 */
	@Property public int DemeSize = 100;
	
	/**
	 * Percent probability that demetic migration will occur for each demetic group, if {@link #DemeticGrouping} is true.<br>
	 * Default: 100.0
	 */
	@Property public double DemeticMigProbability = 100.0;
	
	/**
	 * Determines whether the best individual is automatically added to the next generation, when SteadyState is false. <br>
	 * Default: false
	 *
	 * @see Population#generate(Population)
	 */
	@Property public boolean AddBestToNewPopulation = false;
	
	/**
	 * Determines whether each new generation is created in isolation from the previous generation (SteadyState = false) or
	 * is created by replacing the weaker individuals one by one (SteadyState = true). Steady state operation reduces peak
	 * memory usage by a factor of two.<br>
	 * Default: false
	 *
	 * @see Population#generate(Population)
	 */
	@Property public boolean SteadyState = false;
	
	/**
	 * Determines whether a detail file is generated by GPRun. The detail file includes the population index, heritage,
	 * fitness, complexity, depth, and optionally the s-expression for individuals after each generation.<br>
	 * Default: false
	 */
	@Property public boolean PrintDetails = false;
	
	/**
	 * Determines whether the detail file includes every individual in the population (PrintPopulation = true) or just the
	 * best and worst individuals in the population (PrintPopulation = false).<br>
	 * Default: false
	 */
	@Property public boolean PrintPopulation = false;
	
	/**
	 * Determines whether the detail file shows the s-expression for each selected individual after each generation. Also
	 * determines whether the statistics file includes the s-expression for the best individual of each run.<br>
	 * Default: true
	 *
	 * @see GPRun#showGeneration
	 */
	@Property public boolean PrintExpression = true;
	
	/**
	 * Determines whether the statistics file shows a pseudo-graphic tree for the best individual of each run. Also
	 * determines whether gif files are created showing the best individual of each run in true graphic format.<br>
	 * Default: true
	 *
	 * @see GPRun#showFinalGeneration
	 */
	@Property public boolean PrintTree = true;
	
	/**
	 * The font size in points used to draw the text in graphic trees enabled by PrintTree. The typeface is always Courier, a
	 * monospaced font.<br>
	 * Default: 12
	 */
	@Property public int TreeFontSize = 12;
	
	/**
	 * Sets the number of generations to evolve the population against a random player.<br>
	 * Default: 75
	 */
	@Property public int GenerationsVsRandom = 75;
	
	/**
	 * The timeout for the fjPool in milliseconds.<br>
	 * Default: 5000l;
	 */
	@Property public long ThreadTimeout = 5000l;
	
	/**
	 * The maximum number of timeouts allowed before the machine throws an error.<br>
	 * Default: 1000
	 */
	@Property public int MaxTimeouts = 1000;
	
	/**
	 * The number of games to be played per individual per generation.<br>
	 * Default: 20 <b>Note:</b> This has a significant impact on performance
	 */
	@Property public int NumGames = 20;
	
	/**
	 * The number of players in each game.<br>
	 * Default: 2
	 */
	@Property public int NumPlayers = 2;
	
	/**
	 * This is used in printOn to make the = signs line up.
	 */
	private int fieldNameWidth = 35;
	
	//===================================================================
	
	/**
	 * Public null constructor used to create a set of variables with default values and also during stream loading.
	 */
	public Configuration() { /* All default values are in the field declarations */}
	
	/**
	 * Copy constructor for a {@link Configuration} object
	 * 
	 * @param origial
	 *            the {@link Configuration} to be copied
	 */
	public Configuration(Configuration origial) {
		transferFields(origial, this);
	}
	
	/**
	 * Copies or clones all of the appropriate fields
	 * 
	 * @param source
	 *            the {@link Configuration} instance to copy from
	 * @param destination
	 *            the {@link Configuration} instance to copy to
	 * @param <T>
	 *            used to synchronize the types in the transfer. This will be automatically determined if the method is used
	 *            correctly.
	 */
	public final <T extends Configuration> void transferFields(T source, T destination) {
		try {
			for (Field f : source.getClass().getFields()) {
				f.setAccessible(true); //This is required in order for the field access to not throw IllegalAccessExceptions
				if (f.getName().length() > ((Configuration) destination).fieldNameWidth) //For use in the printing function
					((Configuration) destination).fieldNameWidth = f.getName().length();
				if (f.isAnnotationPresent(Clone.class)) { //If this field should be cloned when the Configuration is cloned, use the copy constructor
					Constructor<?> cons = f.getType().getConstructor(f.getType());
					cons.setAccessible(true);
					f.set(destination, cons.newInstance(f.get(source)));
				}
				//Otherwise, if this is a property or some other value that should be copied, copy it
				else if (f.isAnnotationPresent(Property.class) || f.isAnnotationPresent(Copy.class))
					f.set(destination, f.get(source));
			}
		}
		catch (IllegalArgumentException | ReflectiveOperationException e) {
			System.err.println("Unable to copy all fields.");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Calls {@code new Configuration(this)} and <i>must</i> be overridden by extending classes.
	 *
	 * @return a clone of this {@link Configuration}
	 */
	@Override
	public synchronized Object clone() {
		return new Configuration(this);
	}
	
	/**
	 * Determines whether this set of variables equals another object. It returns true if <tt>other</tt> is not null, is an
	 * instance of {@link Configuration} or a subclass thereof, and contains the same field values. This function is called
	 * when a checkpoint is loaded by {@link GPRun}, to determine whether the program and the checkpoint are consistent. One
	 * can think of various harmless changes made to {@link Configuration} that would still allow a checkpoint to continue
	 * (increasing the number of generations, for example), but this is not allowed here.<br>
	 * This method tests only the public fields annotated with {@link Property}.
	 *
	 * @param other
	 *            any object reference including null; however, this method will only return true if <tt>other</tt> is
	 *            non-null and an instance of {@link Configuration} or a subclass thereof
	 * @return true if this and <tt>other</tt>'s public fields annotated with {@link Property} are equivalent.
	 */
	@Override
	public boolean equals(Object other) {
		if (other == null || !this.getClass().equals(other.getClass()))
			return false;
		try {
			for (Field f : this.getClass().getFields()) {
				f.setAccessible(true); //This is required in order for the field access to not throw IllegalAccessExceptions
				if (f.isAnnotationPresent(Property.class)) {
					Object val = f.get(this);
					if ((val == null && val != f.get(other)) || (val != null && !val.equals(f.get(other))))
						return false;
				}
			}
		}
		catch (IllegalArgumentException | ReflectiveOperationException e) {
			System.err.println("Unable to complete an equality check.");
			System.err.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Loads the values from a {@link Properties} container (read from a configuration file) into the Configuration fields.
	 * If the property strings are invalid, default values for the affected fields remain unchanged. If props is null,
	 * nothing happens.
	 * 
	 * @param props
	 *            the {@link Properties} container to load from
	 */
	@SuppressWarnings("unchecked")
	public synchronized final void load(Properties props) {
		if (props == null)
			return;
		try {
			Property p = null;
			for (Field f : this.getClass().getFields()) {
				f.setAccessible(true); //This is required in order for the field access to not throw IllegalAccessExceptions
				if (f.getName().length() > fieldNameWidth) //For use in the printing function
					fieldNameWidth = f.getName().length();
				if ((p = f.getAnnotation(Property.class)) != null) { //If this field is a property, then we can load it
					if (f.getType().isEnum()) {
						Enum<?> en = (Enum<?>) f.get(this);
						f.set(this, props.get(f.getName(), en, en)); //This converts a String to a specific value in an enum type
					}
					else if (p.selection().length != 0 && f.get(this) instanceof Integer) //If this is an integer index in a String[]-based enum
						f.set(this, props.get(f.getName(), p.selection(), (Integer) f.get(this)));
					else
						//If this is not a Java enum and is not an integer index in a String[]-based enum
						f.set(this, props.get(f.getName(), (Class<Object>) f.getType(), f.get(this)));
				}
			}
		}
		catch (IllegalArgumentException | ReflectiveOperationException e) {
			System.err.println("Unable to load properties.");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Writes the {@link Configuration} fields in a format that can be read by an {@link Properties} object.
	 * 
	 * @param writer
	 *            the {@link PrintWriter} onto which to write the data
	 */
	public void printOn(PrintWriter writer) {
		try {
			Property p = null;
			String propertyFormatString = "%-" + fieldNameWidth + "s = ";
			Field[] fields = this.getClass().getFields();
			Arrays.parallelSort(fields, (a, b) -> a.getName().compareTo(b.getName()));
			for (Field f : fields) {
				f.setAccessible(true); //This is required in order for the field access to not throw IllegalAccessExceptions
				if ((p = f.getAnnotation(Property.class)) != null) { //If this is a Property
					writer.print(String.format(propertyFormatString, f.getName())); //Print the name of the property all nice and pretty
					if (f.getType().isEnum()) //If this is a Java enum
						writer.println(((Enum<?>) f.get(this)).name());
					else if (p.selection().length != 0 && f.get(this) instanceof Integer) //If this is an integer index in a String[]-based enum
						writer.println(p.selection()[(Integer) f.get(this)]);
					else
						//If this is not a Java enum and is not an integer index in a String[]-based enum
						writer.println(arrayToString(f.get(this)));
				}
			}
		}
		catch (IllegalArgumentException | ReflectiveOperationException e) {
			System.err.println("Unable to write properties.");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Recursively constructs a {@link String} representation of an array.
	 * 
	 * @param o
	 *            an {@link Object} of any type, including arrays, or {@code null}
	 * @return "null" if o is {@code null}, {@code o.toString()} if <tt>o</tt> is not an array, or a {@link String}
	 *         representation of the array with recursive calls to {@link #arrayToString(Object)}
	 */
	private String arrayToString(Object o) {
		if (o == null)
			return "null";
		if (!o.getClass().isArray())
			return o.toString();
		final StringBuilder sb = new StringBuilder();
		sb.append("{");
		Arrays.stream((Object[]) o).forEach(i -> sb.append(arrayToString(i)).append(", "));
		if (sb.length() > 1)
			sb.delete(sb.length() - 2, sb.length());
		else
			sb.append(" ");
		return sb.append("}").toString();
	}
}
