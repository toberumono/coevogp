package coevogp.config;

/**
 * Represents the different types of evolution algorithms available to this library.
 * 
 * @author Joshua Lipstone
 * @see Configuration#EvoType
 */
public enum EvolutionType {
	/**
	 * Indicates that the library should perform single population competitive coevolution.<br>
	 * The algorithm for this is parallelized; however, it does not use the Fork-Join framework due to waiting restrictions.
	 * 
	 * @see coevogp.gp.evolvers.SinglePopulationCompetitiveCoevolutionEvolver
	 * @see Configuration#TournamentSize
	 */
	SinglePopulationCompetitiveCoevolution,
	
	/**
	 * Indicates that the library should perform single population evolution.<br>
	 * The selection method used is determined by {@link Configuration#SelType}.
	 * 
	 * @see Configuration#SelType
	 * @see SelectionType
	 * @see Configuration#TournamentSize
	 */
	SinglePopulationEvolution
}
