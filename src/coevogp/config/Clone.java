package coevogp.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that the field this is attached to should be cloned via a copy constructor when cloning the
 * {@link Configuration} instance it is in.<br>
 * All fields marked with this annotation <i>must</i> have a copy constructor that takes a single value of the type of the
 * object being cloned and is declared as public.
 * 
 * @author Joshua Lipstone
 * @see Property
 * @see Copy
 * @see Configuration
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Clone {}
