package coevogp.config;

/**
 * A helper interface for {@link Properties#get(String, Class, Object)}. Converts a property from type <tt>T</tt> to
 * <tt>R</tt>
 * 
 * @author Joshua Lipstone
 * @param <T>
 *            the initial type of the property value
 * @param <R>
 *            the resulting type of the property value
 */
@FunctionalInterface
public interface PropertyConverter<T, R> {
	public R convert(T property, Object def);
}