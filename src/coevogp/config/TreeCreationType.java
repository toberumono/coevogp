package coevogp.config;

/**
 * The available methods of tree creation
 * 
 * @author Joshua Lipstone
 */
public enum TreeCreationType {
	/**
	 * A tree creation type that selects each non-root node with a 50:50 chance of becoming a function or terminal and that
	 * continues building every tree to {@link Configuration#MaximumDepthForCreation}.
	 */
	Variable {
		@Override
		public TreeCreationType use(int i) {
			return this;
		}
	},
	
	/**
	 * A tree creation type that makes every node but those on the bottom level a function and that builds every tree to
	 * {@link Configuration#MaximumDepthForCreation}.
	 */
	Grow {
		@Override
		public TreeCreationType use(int i) {
			return this;
		}
	},
	
	/**
	 * A tree creation type that builds alternating trees using the {@link TreeCreationType#RampedVariable} and
	 * {@link TreeCreationType#RampedGrow} methods. This is the default method.
	 */
	RampedHalf {
		@Override
		public TreeCreationType use(int i) {
			return i % 2 == 0 ? Variable : Grow;
		}
	},
	
	/**
	 * A tree creation type that is like {@link TreeCreationType#Variable} but starts with a tree depth of two and increases
	 * the depth by one for each tree until it reaches {@link Configuration#MaximumDepthForCreation}, at which point it
	 * cycles back to two.<br>
	 * Note that if {@link Configuration#TestDiversity} is enabled many of the shallow trees are duplicates, so the
	 * distribution is skewed toward deeper trees than one might expect.
	 */
	RampedVariable {
		@Override
		public TreeCreationType use(int i) {
			return this;
		}
	},
	
	/**
	 * A tree creation type that is like {@link TreeCreationType#RampedGrow} but starts with a tree depth of two and
	 * increases the depth by one for each tree until it reaches {@link Configuration#MaximumDepthForCreation}, at which
	 * point it cycles back to two.<br>
	 * Note that if {@link Configuration#TestDiversity} is enabled many of the shallow trees are duplicates, so the
	 * distribution is skewed toward deeper trees than one might expect.
	 */
	RampedGrow {
		@Override
		public TreeCreationType use(int i) {
			return this;
		}
	};
	
	/**
	 * Returns the creation method to use based on the index of the GP being created
	 * 
	 * @param i
	 *            the index of the GP being created
	 * @return the creation method to use
	 */
	public abstract TreeCreationType use(int i);
}
