package coevogp.gp.evolvers;

import java.util.function.Supplier;

import coevogp.Game;
import coevogp.Player;
import coevogp.config.Configuration;
import coevogp.gp.GP;
import coevogp.gp.NodeSet;
import coevogp.gp.Population;

/**
 * A container for an evolution algorithm.
 * 
 * @author Joshua Lipstone
 * @param <C>
 *            the type of {@link Configuration} to use
 * @param <Pl>
 *            the type of {@link Player} to use
 */
public abstract class Evolver<C extends Configuration, Pl extends Player<C>, R> {
	protected final C cfg;
	protected final Game<C, Pl, R> game;
	protected final Supplier<GP<C, R>> createGP;
	protected final NodeSet<C, R> ns;
	
	/**
	 * Constructs a new {@link Evolver} with the given configuration
	 * 
	 * @param cfg
	 *            the configuration to use with the {@link Evolver}
	 * @param game
	 *            the game to use with the {@link Evolver}
	 * @param createGP
	 *            a function that produces new {@link GP GPs}
	 * @param ns
	 *            the {@link NodeSet} to use with the {@link Evolver}
	 */
	public Evolver(C cfg, Game<C, Pl, R> game, Supplier<GP<C, R>> createGP, NodeSet<C, R> ns) {
		this.cfg = cfg;
		this.game = game;
		this.createGP = createGP;
		this.ns = ns;
	}
	
	/**
	 * This method uses the appropriate algorithm to evolve the given population and place the results in either <tt>pop</tt>
	 * or <tt>newPop</tt> as appropriate.
	 * 
	 * @param pop
	 *            the population representing the current generation
	 * @param newPop
	 *            the population into which to insert the results, or ignore if {@link Configuration#SteadyState} is
	 *            {@code true}
	 * @param numGens
	 *            the number of generations for which this algorithm should evolve the population. This parameter is ignored
	 *            for some evolution methods.
	 * @return newPop, but with the results from evolving the population
	 */
	public abstract Population<C, Pl, R> evolve(Population<C, Pl, R> pop, Population<C, Pl, R> newPop, int numGens);
}
