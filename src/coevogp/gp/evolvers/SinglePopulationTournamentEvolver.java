package coevogp.gp.evolvers;

import java.util.concurrent.RecursiveAction;
import java.util.function.Supplier;

import coevogp.Container;
import coevogp.Game;
import coevogp.Player;
import coevogp.config.Configuration;
import coevogp.gp.GP;
import coevogp.gp.GPRoot;
import coevogp.gp.NodeSet;
import coevogp.gp.Population;

import static coevogp.Helpers.*;

public class SinglePopulationTournamentEvolver<C extends Configuration, Pl extends Player<C>, R> extends Evolver<C, Pl, R> {
	private GPRoot[] working;
	private static final int threshold = 200;
	private int tSize, lim;
	
	public SinglePopulationTournamentEvolver(C cfg, Game<C, Pl, R> game, Supplier<GP<C, R>> createGP, NodeSet<C, R> ns) {
		super(cfg, game, createGP, ns);
		tSize = cfg.TournamentSize;
	}
	
	class Tournament extends RecursiveAction {
		private final int s, e;
		private final C cfg;
		private final GPRoot[] result;
		
		public Tournament(C cfg, GPRoot[] result) {
			this.cfg = cfg;
			this.result = result;
			s = 0;
			e = result.length;
			
		}
		
		Tournament(C cfg, GPRoot[] result, int s, int e) {
			this.cfg = cfg;
			this.result = result;
			this.s = s;
			this.e = e;
		}
		
		@Override
		@SuppressWarnings("unchecked")
		protected void compute() {
			if (e - s > threshold) {
				int mid = (s + e) / 2;
				//We only need to clone cfg once
				invokeAll(new Tournament(cfg, result, s, mid), new Tournament((C) cfg.clone(), result, mid, e));
				return;
			}
			int test = s % 2;
			for (int i = s; i < e;) {
				GP<C, R> best = null;
				double bestFitness = Double.MAX_VALUE;
				for (int j = 0, c = 0; j < lim && c < tSize; j++) {
					if (rgen.nextDouble() <= ((double) (tSize - c)) / (lim - j)) {
						c++;
						if (((GP<?, R>) working[j]).getFitness() < bestFitness) {//Pick and store the best individual so far
							best = (GP<C, R>) working[j];
							bestFitness = best.getFitness();
						}
					}
				}
				result[i] = (GPRoot) best.clone();
				if (++i % 2 == test) {
					int l = i - 1;
					Container<GP<C, R>> res = ((GP<C, R>) result[l]).cross((GP<C, R>) result[l - 1], cfg.MaximumDepthForCrossover, cfg.MaximumComplexity);
					result[l - 1] = (GPRoot) res.get(0).mutate(cfg, ns);;
					result[l] = (GPRoot) res.get(1).mutate(cfg, ns);;
				}
			}
		}
	}
	
	@Override
	public Population<C, Pl, R> evolve(Population<C, Pl, R> pop, Population<C, Pl, R> newPop, int numGens) {
		Pl[] arr = game.makeArray(1);
		for (int i = 0; i < numGens; i++) {
			working = pop.getBacking();
			for (GPRoot e : working) {
				@SuppressWarnings("unchecked")
				GP<C, R> gp = (GP<C, R>) e;
				arr[0] = game.makePlayer(1, gp::evaluate);
				gp.setFitness(game.play(cfg, arr)[0]);
			}
			lim = working.length;
			fjPool.invoke(new Tournament(cfg, newPop.getBacking()));
			if (!cfg.SteadyState) {
				//swap the pops
				Population<C, Pl, R> tmp = pop;
				pop = newPop;
				newPop = tmp;
				//ensure no references remain to obsolete GPs
				newPop.clear();
			}
			System.out.println("gen: " + i);
			//TODO print current generation data
		}
		pop.calculateStatistics();
		return pop; //TODO work out SteadyState
	}
	
}
