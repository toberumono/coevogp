package coevogp.gp.evolvers;

import java.util.Arrays;
import java.util.function.Supplier;

import coevogp.Container;
import coevogp.Game;
import coevogp.Helpers;
import coevogp.Player;
import coevogp.config.Configuration;
import coevogp.gp.GP;
import coevogp.gp.GPRoot;
import coevogp.gp.NodeSet;
import coevogp.gp.Population;
import concurrency.Connector;
import concurrency.Operator;

import static coevogp.Helpers.rgen;

public class SinglePopulationCompetitiveCoevolutionEvolver<C extends Configuration, Pl extends Player<C>, R> extends Evolver<C, Pl, R> {
	
	public SinglePopulationCompetitiveCoevolutionEvolver(C cfg, Game<C, Pl, R> game, Supplier<GP<C, R>> createGP, NodeSet<C, R> ns) {
		super(cfg, game, createGP, ns);
	}

	final class Tournament extends Operator<GP<C, R>> {
		private final C cfg;
		private final int np;
		private final Pl[] arr;
		private GP<C, R> bestPlayer;
		private double bestFitness;
		private static final String numberFormatter = "%.2f";
		
		@SuppressWarnings("unchecked")
		public Tournament(Object[] structure, int start, int end, int maxGens, Connector<GP<C, R>> source, Connector<GP<C, R>> destination, boolean preFilled, C cfg) {
			super(structure, start, end, maxGens, source, destination, preFilled);
			this.cfg = (C) cfg.clone();
			np = cfg.NumPlayers;
			arr = game.makeArray(np);
			bestPlayer = null;
			bestFitness = Double.MAX_VALUE;
		}
		
		@Override
		protected void process() {
			double[] fitness = new double[np];
			if (generation >= cfg.GenerationsVsRandom) { //Play vs each other
				for (int g = 0; g < cfg.NumGames; g++) {
					Helpers.scrambleInRange(start, end, structure); //Randomize the combatants 
					for (int i = start; i < end; i += np) {
						for (int p = 0; p < np; p++)
							arr[p] = game.makePlayer(p + 1, get(i + p)::evaluate);
						fitness = game.play(cfg, arr);
						for (int p = 0; p < np; p++)
							get(i + p).updateFitness(fitness[p]);
					}
				}
				double fit;
				for (int i = start; i < end; i++) {
					fit = get(i).getFitness();
					if (fit < bestFitness) {
						bestPlayer = get(i);
						bestFitness = fit;
					}
					this.supply(get(i));
				}
			}
			else { //Play vs Random
				double fit;
				for (int i = start; i < end; i++) {
					GP<C, R> p = get(i);
					Pl player = game.makePlayer(1, p::evaluate); //The same player will be used several times, so we save it here
					for (int g = 0; g < cfg.NumGames; g++)
						p.updateFitness(game.playRandom(cfg, player));
					fit = p.getFitness();
					if (fit < bestFitness) {
						bestPlayer = p;
						bestFitness = fit;
					}
					this.supply(p);
				}
			}
		}
		
		@Override
		protected void reset() {
			super.reset();
			bestPlayer = null;
			bestFitness = Double.MAX_VALUE;
		}
		
		@Override
		public String generationDataToString() {
			//Plays the best individual from the generation against random and appends the win rate to the original generation data
			double wins = 0.0;
			Pl player = game.makePlayer(1, bestPlayer::evaluate);
			for (int i = 0; i < cfg.NumGames; i++)
				wins += game.playRandomWon(cfg, player);
			return super.generationDataToString() + ": Best vs Random: Win Rate: " + String.format(numberFormatter, wins / cfg.NumGames * 100) + "%";
		}
	}
	
	final class Mutate extends Operator<GP<C, R>> {
		private final int tSize;
		
		public Mutate(Object[] structure, int start, int end, int maxGens, Connector<GP<C, R>> source, Connector<GP<C, R>> destination, boolean preFilled) {
			super(structure, start, end, maxGens, source, destination, preFilled);
			tSize = cfg.TournamentSize / 2;
			this.setPrintGenerationData(false);
		}
		
		@Override
		@SuppressWarnings("unchecked")
		protected void process() {
			Container<GP<C, R>> parents = new Container<>(2);
			//Performs tournament selection to pick individuals that should pass on their genes.
			for (int i = 0, index = i % 2; i < capacity; index = i % 2) {
				for (int j = start, c = 0; j < end && c < tSize; j++) {
					if (rgen.nextDouble() <= ((double) (tSize - c)) / (end - j)) {
						c++;
						if (parents.get(index) == null || parents.get(index).getFitness() > get(j).getFitness()) //Pick and store the best individual so far
							parents.set(index, get(j));
					}
				}
				//For every two individuals, perform crossover and mutation
				if (++i % 2 == 0) {
					for (GP<C, R> p : evolve((Container<GP<C, R>>) parents.clone())) {
						p.setFitness(0.0);
						this.supply(p);
					}
					parents.clear();
				}
			}
		}
		
		private Container<GP<C, R>> evolve(Container<GP<C, R>> parents) {
			Container<GP<C, R>> result = parents;
			if (rgen.flip(cfg.CrossoverProbability)) {
				result = parents.get(0).cross(parents.get(1), cfg.MaximumDepthForCrossover, cfg.MaximumComplexity);
			}
			else if (rgen.flip(cfg.CreationProbability)) {
				result.set(0, createGP.get());
				result.get(0).create(cfg.CreationType, cfg.MaximumDepthForCreation, cfg.MaximumComplexity, ns);
				result.set(1, createGP.get());
				result.get(1).create(cfg.CreationType, cfg.MaximumDepthForCreation, cfg.MaximumComplexity, ns);
			}
			for (GP<C, R> p : result)
				p.mutate(cfg, ns);
			return result;
		}
	}

	@Override
	public Population<C, Pl, R> evolve(Population<C, Pl, R> pop, Population<C, Pl, R> newPop, int numGens) {
		GPRoot[] working = Arrays.copyOf(pop.getBacking(), pop.capacity()); //The array used as the structure for the mutators
		Connector<GP<C, R>> tournament = new Connector<>();
		Connector<GP<C, R>> mutation = new Connector<>();
		//I know this looks crazy, but it just describes the connections between the Operators and Connectors as well as flagging which set of Operators starts full
		tournament.initialize((structure, s, e, source, pf) -> new Tournament(structure, s, e, numGens, source, mutation, pf, cfg), pop.getBacking(), cfg.TournamentSize, true);
		mutation.initialize((structure, s, e, source, pf) -> new Mutate(structure, s, e, numGens, source, tournament, pf), working, cfg.TournamentSize, false);
		mutation.start();
		tournament.start();
		try {
			tournament.join();
			//mutation.join();
			mutation.forceStop();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (!cfg.SteadyState) {
			//clear the duplicate checker
			newPop.clearUniqueGPs();
			
			//if requested, take best and add to new generation
			pop.addBest(newPop);
		}
		for (int i = 0; i < pop.capacity(); i++)
			newPop.set(i, pop.get(i));
		newPop.calculateStatistics();
		pop.calculateStatistics();
		return newPop;
	}
}
