package coevogp.gp;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import Acme.JPM.Encoders.GifEncoder;

public class Drawing extends Frame {
	/**
	 * The typeface used to display text in genetic trees. This should be a monospace font. Courier was chosen as a default
	 * because it is a standard Java font that should be available on all platforms. There's nothing that limits you to just
	 * one font for other applications, but GPDrawing enables this one and gets various font metrics that are useful while
	 * drawing.
	 */
	public static String fontName = "Courier";
	
	/**
	 * The default font size used for the font. This can be changed while constructing the object or by calling
	 * {@link #setFontSize(int)}. Default value 12.
	 */
	public static int defFontSize = 12;
	
	/**
	 * The canvas on which the image is drawn.
	 */
	protected Canvas cvs;
	
	/**
	 * The active font for the image.
	 */
	protected Font fnt;
	
	/**
	 * Font metrics for the active font.
	 */
	protected FontMetrics fm;
	
	/**
	 * An off-screen image associated with the canvas on which the drawing is created.
	 */
	protected Image img;
	
	/**
	 * A graphics context whose methods are called to produce the off-screen image.
	 *
	 * @see #getGra
	 */
	protected Graphics gra;
	
	/**
	 * The current font size.
	 */
	protected int fontSize;
	
	/**
	 * The width of the widest character in the current font. It's public so that users of the class can get the value
	 * efficiently.
	 */
	public int cw;
	
	/**
	 * The height of the current font. It's public so that users of the class can get the value efficiently.
	 */
	public int ch;
	
	/**
	 * The maximum ascent of any character in the current font. It's public so that users of the class can get the value
	 * efficiently.
	 */
	public int as;
	
	/**
	 * Public null constructor for this class. It creates a Frame with the title "gif drawing", adds a Canvas to it, creates
	 * the peer components, creates the default font at default size, and gets the {@link Drawing#cw}, {@link Drawing#ch},
	 * and {@link Drawing#as} font metrics. The frame remains hidden and disabled.
	 */
	public Drawing() {
		super("gif drawing");
		
		//create a canvas to draw on
		cvs = new Canvas();
		
		//add canvas to frame
		add(cvs);
		
		//create peer components
		addNotify();
		
		//create a default font
		setFontSize(defFontSize);
	}
	
	/**
	 * Changes the font size. The typeface is specified by the {@link #fontName} field of the class. Updates the {@link #fm},
	 * {@link #cw}, {@link #ch}, and {@link #as} fields of the class.
	 * 
	 * @param fontSize
	 *            the font size to use
	 */
	public void setFontSize(int fontSize) {
		if (fontSize != this.fontSize) {
			this.fontSize = fontSize;
			fnt = new Font(fontName, Font.PLAIN, fontSize);
			if (fnt == null)
				throw new RuntimeException("Courier font not available");
			
			//get font metrics needed by gif tree
			fm = getFontMetrics(fnt);
			cw = fm.getMaxAdvance();
			ch = fm.getHeight();
			as = fm.getMaxAscent();
		}
	}
	
	/**
	 * Prepares an off-screen drawing image of the specified pixel width and height. The image is cleared to a white
	 * background, the default font is enabled, and the foreground color is set to black.
	 * 
	 * @param w
	 *            the width
	 * @param h
	 *            the height
	 */
	public void prepImage(int w, int h) {
		prepImage(w, h, this.fontSize);
	}
	
	/**
	 * Sets a new font size and prepares an off-screen drawing image.
	 *
	 * @param w
	 *            the width
	 * @param h
	 *            the height
	 * @param fontSize
	 *            the font size
	 * @see #prepImage(int, int)
	 */
	public void prepImage(int w, int h, int fontSize) {
		//create properly sized font if needed
		setFontSize(fontSize);
		
		//create image of desired size
		img = cvs.createImage(w, h);
		if (img == null)
			throw new RuntimeException("Unable to create image");
		if (gra != null)
			gra.dispose();
		gra = img.getGraphics();
		
		//clear the image and set default colors and font
		gra.setColor(Color.white);
		gra.fillRect(0, 0, w, h);
		gra.setColor(Color.black);
		gra.setFont(fnt);
	}
	
	/**
	 * Returns the pixel width of a text string when displayed in the default font.
	 * 
	 * @param s
	 *            the {@link String} to get the pixel width of
	 * @return the width of <tt>s</tt> in pixels when displayed in the default font
	 */
	public int stringWidth(String s) {
		return fm.stringWidth(s);
	}
	
	/**
	 * Returns the graphics context on which drawing is done. See {@link Graphics} for a complete list of available drawing
	 * methods. {@link #prepImage(int, int)} must be called first.
	 * 
	 * @return the {@link Graphics} context on which the drawing is done
	 */
	public Graphics getGra() {
		return gra;
	}
	
	/**
	 * Writes the current off-screen image to a gif file.
	 *
	 * @param fname
	 *            the name of the gif file to create.
	 * @exception IOException
	 *                if an error occurs while writing the file.
	 */
	public void writeGif(String fname) throws IOException {
		
		OutputStream writer = new BufferedOutputStream(
				new FileOutputStream(fname), 1024);
		(new GifEncoder(img, writer)).encode();
		writer.close();
	}
}
