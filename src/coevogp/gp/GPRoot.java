package coevogp.gp;

/**
 * A convenience class to make it easier to access {@link #clone()} and {@link #testNull()} within the rest of this library.
 * 
 * @author Joshua Lipstone
 */
public interface GPRoot extends Cloneable {
	
	public Object clone();
	
	/**
	 * This method is declared here for convenience. However, it is mainly used in {@link coevogp.Container Container} and
	 * its subclasses.
	 * An appropriate implementation of this method for objects that only extend {@link GPRoot} is {@code return false;}.
	 * 
	 * @return true if this {@link GPRoot} contains a {@code null} value
	 */
	public boolean testNull();
}
