package coevogp.gp;

import java.io.PrintWriter;
import java.util.HashMap;

import coevogp.Container;
import coevogp.GPRun;
import coevogp.Game;
import coevogp.Player;
import coevogp.config.Configuration;
import coevogp.config.EvolutionType;
import coevogp.config.SelectionType;
import coevogp.config.TreeCreationType;
import coevogp.exceptions.InvalidEASpecificationException;
import coevogp.gp.evolvers.Evolver;
import coevogp.gp.evolvers.SinglePopulationCompetitiveCoevolutionEvolver;
import coevogp.gp.evolvers.SinglePopulationTournamentEvolver;
import static coevogp.config.SelectionType.*;
import static coevogp.config.TreeCreationType.*;

/**
 * Represents a population of individuals where each individual is a {@link GP}
 * 
 * @author Joshua Lipstone
 * @param <C>
 *            the type of {@link Configuration} to be used
 * @param <Pl>
 *            the type of {@link Player} to be used
 */
public class Population<C extends Configuration, Pl extends Player<C>, R> extends Container<GP<C, R>> {
	
	/**
	 * Specifies the minimum tree depth for newly created individuals. Default value 2.
	 */
	protected static int minTreeDepth = 2;
	
	/**
	 * The sum of the adjusted fitness over an entire range. Used for probabilistic and greedy selection only.
	 */
	protected double sumAdjFitness;
	
	/**
	 * The cutoff fitness for an individual to get into group I. Used for greedy selection only.
	 */
	protected double cutoffAdjFitness;
	
	/**
	 * The sum of the adjusted fitness for group I individuals. Used for greedy selection only.
	 */
	protected double sumG1AdjFitness;
	
	/**
	 * The sum of the adjusted fitness for group II individuals. Used for greedy selection only.
	 */
	protected double sumG2AdjFitness;
	
	/**
	 * The sum of the standardized fitness over an entire range.
	 */
	protected double sumFitness;
	
	/**
	 * A reference to the global configuration variables for the run.
	 */
	protected C cfg;
	
	/**
	 * A reference to the functions and terminals for the run.
	 */
	protected NodeSet<C, R> ns;
	
	/**
	 * A reference to the game used to determine fitnesses for the run.
	 */
	protected Game<C, Pl, R> game;
	
	/**
	 * Average fitness of the entire population. Used in probabilistic selection and reported by printStatistics().
	 * Calculated in calculateStatistics().
	 */
	protected double avgFitness;
	
	/**
	 * Average complexity of the entire population. Reported by printStatistics() and calculated in calculateStatistics().
	 */
	protected double avgComplexity;
	
	/**
	 * Average tree depth of the entire population. Reported by printStatistics() and calculated in calculateStatistics().
	 */
	protected double avgDepth;
	
	/**
	 * Best standardized fitness found in the entire population. Reported by printStatistics() and calculated in
	 * calculateStatistics().
	 */
	public double bestFitness;
	
	/**
	 * Worst standardized fitness found in the entire population. Reported by printStatistics() and calculated in
	 * calculateStatistics().
	 */
	public double worstFitness;
	
	/**
	 * The index of the individual with the best standardized fitness. In case of ties, the lower complexity wins. The GP
	 * associated with this index is obtained by <code>pop.get(bestOfPopulation)</code>.
	 */
	public int bestOfPopulation;
	
	/**
	 * The index of the individual with the worst standardized fitness. In case of ties, the higher complexity wins. The GP
	 * associated with this index is obtained by <code>pop.get(worstOfPopulation)</code>.
	 */
	public int worstOfPopulation;
	
	/**
	 * The highest complexity found anywhere in the population. This may differ from the complexity of the individual that
	 * has the worst fitness. longestComplexity is calculated in printStatistics() but is not printed in any standard
	 * reports.
	 */
	public int longestComplexity;
	
	/**
	 * A table of the unique GPs found in the population. This table is created and updated only if the
	 * {@link Configuration#TestDiversity} configuration option is enabled. Each unique GP is entered in the table along with
	 * a count of the times it appears in the population. When TestDiversity is enabled, the initial population is guaranteed
	 * to be 100% diverse by rejecting any individuals that already appear in the table. For successive generations,
	 * duplicates are allowed but the diversity is reported by printStatistics(). The HashMap approach to tracking diversity
	 * is fast enough that the option can be left on at all times.
	 */
	protected HashMap<GP<C, R>, Integer> uniqueGPs;
	
	/**
	 * The number of duplicate individuals in the current generation. For the initial generation, this number is always 0.
	 * dupCount is not printed in any standard reports, but the diversity ratio (1-dupCount/popSize) is reported by
	 * printStatistics().
	 */
	public int dupCount;
	
	/**
	 * The number of duplicate individuals that were created in generation 0 but rejected. When diversity checking is enabled
	 * generation 0 is guaranteed to be 100% diverse. attemptedDupCount is printed among the standard output of the
	 * {@link GPRun} class.
	 */
	public int attemptedDupCount;
	
	/**
	 * The number of individuals that were created for generation 0 but rejected because their complexity exceeded the global
	 * configuration variable {@link Configuration#MaximumComplexity}. attemptedComplexCount is printed among the standard
	 * output of the {@link GPRun} class.
	 */
	public int attemptedComplexCount;
	
	/**
	 * Constructor used when populations are first created. This constructor creates a container capable of holding the
	 * population individuals, but does not create the individuals. It also performs a number of consistency checks on the
	 * configuration variables and node set.
	 *
	 * @param cfg
	 *            the global configuration variables
	 * @param ns
	 *            the set of node types for all branches
	 * @param game
	 *            the {@link Game} to use in coevolution. This is ignored if the evolution method does not utilize
	 *            coevolution.
	 * @throws RuntimeException
	 *             if any problems are found in the configuration. The exception message provides more details.
	 * @see #create()
	 */
	public Population(C cfg, NodeSet<C, R> ns, Game<C, Pl, R> game) {
		super(cfg.PopulationSize);
		
		//make sure there's something to do
		if (cfg.NumberOfGenerations < 1)
			throw new RuntimeException("Number of generations must be at least 1");
		if (cfg.PopulationSize < 2)
			throw new RuntimeException("Population size must be at least 2");
		
		//check for possibility of infinite loop during crossover
		if (cfg.MaximumDepthForCrossover < cfg.MaximumDepthForCreation)
			throw new RuntimeException("MaximumDepthForCrossover cannot be less than MaximumDepthForCreation");
		
		//check tournament selection parameters
		if (cfg.SelType == TournamentSelection)
			if (cfg.TournamentSize < 2)
				throw new RuntimeException("Tournament size must be at least 2");
		
		//disable demetic grouping for greedy over-selection
		if (cfg.SelType == GreedySelection)
			cfg.DemeticGrouping = false;
		
		//check demetic grouping parameters
		if (cfg.DemeticGrouping) {
			int demeSize = cfg.DemeSize;
			int popSize = cfg.PopulationSize;
			if (demeSize < 2)
				throw new RuntimeException("Demetic group size must be greater than 1");
			if (demeSize > popSize)
				throw new RuntimeException("Demetic group size must not exceed population size");
			if (popSize % demeSize != 0)
				throw new RuntimeException("Population must be an integer multiple of deme size");
		}
		
		//confirm that the user has initialized the node set correctly.
		if ((ns == null) || (ns.capacity() == 0))
			throw new RuntimeException("Node set is undefined");
		if (ns.numFunctions() == 0)
			throw new RuntimeException("No functions are available");
		if (ns.numTerminals() == 0)
			throw new RuntimeException("No terminals are available");
		for (int i = 0; i < ns.capacity(); i++)
			if (ns.get(i) == null)
				throw new RuntimeException("Node set is missing functions or terminals");
		
		//save configuration and node set references
		this.cfg = cfg;
		this.ns = ns;
		this.game = game;
		
		//allocate diversity checker
		clearUniqueGPs();
	}
	
	/**
	 * A constructor that can be called to clone a population. Normally not used.
	 * 
	 * @param gpo
	 *            the {@link Population} to clone
	 */
	public Population(Population<C, Pl, R> gpo) {
		super(gpo);
		
		cfg = gpo.cfg;
		avgComplexity = gpo.avgComplexity;
		avgDepth = gpo.avgDepth;
		
		//compute the diversity
		buildUniqueGPs();
	}
	
	/**
	 * Implements the Cloneable interface. This clones a Population but is normally not used.
	 *
	 * @return the cloned object.
	 */
	@Override
	public synchronized Object clone() {
		return new Population<>(this);
	}
	
	/**
	 * Creates the GP used for a new individual. The user must override this in a subclass to create GPs of user type. See
	 * the example programs.
	 *
	 * @return the newly created GP.
	 */
	public GP<C, R> createGP() {
		return new GP<>(1);
	}
	
	/**
	 * Clears the diversity table and calls updateUniqueGPs for each individual in the population. If
	 * {@link Configuration#TestDiversity} is false, does nothing.
	 */
	protected void buildUniqueGPs() {
		if (cfg.TestDiversity) {
			clearUniqueGPs();
			for (int i = 0; i < capacity(); i++)
				updateUniqueGPs(get(i));
		}
	}
	
	/**
	 * Computes the size of the hash table used for diversity checking. It equals an odd number just larger than twice the
	 * population size, which should generally avoid having to expand the diversity table after it is created.
	 * 
	 * @return the size of the hash table used for diversity checking
	 */
	protected int getUniqueGPsSize() {
		return (2 * cfg.PopulationSize + 1);
	}
	
	/**
	 * Creates or clears the diversity table and the {@link #dupCount} and {@link #attemptedDupCount} fields. If
	 * {@link Configuration#TestDiversity} is false, does nothing.
	 */
	public synchronized void clearUniqueGPs() {
		if (cfg.TestDiversity) {
			if (uniqueGPs == null)
				uniqueGPs = new HashMap<>(getUniqueGPsSize());
			else
				uniqueGPs.clear();
			dupCount = 0;
			attemptedDupCount = 0;
		}
	}
	
	/**
	 * Adds the specified {@link GP} to the diversity table, or increments its count if already in the table. If
	 * {@link Configuration#TestDiversity cfg.TestDiversity} is false, does nothing.
	 * 
	 * @param gp
	 *            the {@link GP} to add
	 */
	protected void updateUniqueGPs(GP<C, R> gp) {
		if (cfg.TestDiversity) {
			if (uniqueGPs.containsKey(gp)) {
				uniqueGPs.put(gp, uniqueGPs.get(gp) + 1);
				dupCount++;
			}
			else
				uniqueGPs.put(gp, 1);
		}
	}
	
	/**
	 * Returns true if the specified {@link GP} is not already in the diversity table or if
	 * {@link Configuration#TestDiversity cfg.TestDiversity} is false. If the {@link GP} is already in the table, returns
	 * false and increments {@link #attemptedDupCount}. Used when creating an initial population.
	 * 
	 * @param gp
	 *            the {@link GP} to check
	 * @return true if the specified {@link GP} is not already in the diversity table or if
	 *         {@link Configuration#TestDiversity cfg.TestDiversity}
	 */
	protected boolean checkForDiversity(GP<C, R> gp) {
		if (cfg.TestDiversity && (gp != null)) {
			if (uniqueGPs.containsKey(gp)) {
				uniqueGPs.put(gp, uniqueGPs.get(gp) + 1);
				attemptedDupCount++;
				return false;
			}
			uniqueGPs.put(gp, 1);
		}
		return true;
	}
	
	/**
	 * Adds the best individual from this generation to the next. This method does nothing unless the configuration variable
	 * {@link Configuration#AddBestToNewPopulation} is true. It can be called only when a steady state population is not in
	 * use and therefore newPop exists.
	 *
	 * @param newPop
	 *            the population for the next generation.
	 */
	public synchronized void addBest(Population<C, Pl, R> newPop) {
		if (cfg.AddBestToNewPopulation) {
			@SuppressWarnings("unchecked")
			GP<C, R> newGP = (GP<C, R>) get(bestOfPopulation).clone();
			newGP.dadIndex = bestOfPopulation;
			newPop.set(bestOfPopulation, newGP);
			newPop.updateUniqueGPs(newGP);
		}
	}
	
	public synchronized GP<C, R> getBestOfPop() {
		return get(bestOfPopulation);
	}
	
	/**
	 * This method, depending on the algorithm, depending on the method of evolution selected evolves the given population
	 * some number of generations.
	 * 
	 * @param newPop
	 *            the {@link Population} into which the results should be placed if {@link Configuration#SteadyState} is
	 *            {@code false}
	 * @return the {@link Population} representing the resulting generation.
	 */
	public Population<C, Pl, R> generate(Population<C, Pl, R> newPop) {
		if (cfg.EvoType == EvolutionType.SinglePopulationCompetitiveCoevolution) {
			Evolver<C, Pl, R> evo = new SinglePopulationCompetitiveCoevolutionEvolver<>(cfg, game, this::createGP, ns);
			return evo.evolve(this, newPop, cfg.NumberOfGenerations);
		}
		if (cfg.EvoType == EvolutionType.SinglePopulationEvolution) {
			if (cfg.SelType == SelectionType.TournamentSelection) {
				Evolver<C, Pl, R> evo = new SinglePopulationTournamentEvolver<>(cfg, game, this::createGP, ns);
				return evo.evolve(this, newPop, cfg.NumberOfGenerations);
			}
		}
		throw new InvalidEASpecificationException("Cannot match " + cfg.EvoType.name() + " and " + cfg.SelType.name() + " to a valid EA.");
	}
	
	/**
	 * Creates all of the individuals in an initial population. If cfg.TestForDiversity is true, tries up to 50 times per
	 * individual to create a unique GP. Also tries up to 50 times per individual to create GPs whose complexity is less than
	 * cfg.MaximumComplexity and increments attemptedComplexCount for each individual that fails.
	 * <p>
	 * The depth of each GP is guaranteed to fall in the range GPPopulation.minTreeDepth (2) to cfg.MaximumDepthForCreation
	 * (6 by default).
	 * <p>
	 * create() calls <a href="gpjpp.GP.html#create">GP.create()</a> to create each individual in the population.
	 * <p>
	 * create() uses one of several tree-building strategies depending on the value of the configuration variable <a
	 * href="gpjpp.GPVariables.html#CreationType">CreationType</a>. If this variable equals GPRAMPEDHALF (the default),
	 * alternating individuals are created using GPGROW (function nodes to maximum depth) and GPVARIABLE (nodes chosen with a
	 * 50:50 probability of being a function or a terminal). With GPRAMPEDHALF, the depth of successive individuals is ramped
	 * from minTreeDepth to MaximumDepthForCreation and back again.
	 * <p>
	 * If CreationType equals GPRAMPEDVARIABLE, all individuals are created using the GPVARIABLE strategy and the depth of
	 * successive individuals is ramped from minTreeDepth to MaximumDepthForCreation.
	 * <p>
	 * If CreationType equals GPRAMPEDGROW, all individuals are created using the GPGROW strategy and the depth of successive
	 * individuals is ramped.
	 * <p>
	 * If CreationType equals GPGROW, all individuals are created using the GPGROW strategy with depth
	 * MaximumDepthForCreation.
	 * <p>
	 * If CreationType equals GPVARIABLE, all individuals are created using the GPVARIABLE strategy with depth
	 * MaximumDepthForCreation.
	 * <p>
	 * If a unique individual is not found in 50/4 tries, the tree depth is incremented for each try thereafter, up to a
	 * maximum of MaximumDepthForCreation.
	 * <p>
	 * If an individual of acceptable complexity is not found in 50/4 tries, the tree depth is decremented for each try
	 * thereafter, down to a minimum of minTreeDepth.
	 * <p>
	 * If no acceptable individual is found after 50 tries, a RuntimeException is thrown.
	 * <p>
	 * After each individual is created, its fitness is calculated by calling <a
	 * href="gpjpp.GP.html#evaluate">GP.evaluate()</a>. After all individuals are created, <a
	 * href="gpjpp.GPPopulation.html#calculateStatistics"> calculateStatistics()</a> is called.
	 */
	public void create() {
		
		//clear diversity checker
		clearUniqueGPs();
		attemptedComplexCount = 0;
		
		int treeDepth = minTreeDepth;
		int creationTries = 50;
		
		for (int i = 0; i < capacity(); i++) {
			
			//create a new GP, or the user's subclass of it
			GP<C, R> newGP = createGP();
			
			//compute desired creation type and tree depth
			TreeCreationType creationType = cfg.CreationType.use(i);
			int depth = cfg.CreationType == Grow || cfg.CreationType == Variable ? cfg.MaximumDepthForCreation : treeDepth;
			
			trying: {
				//attempt to build a unique tree of allowable complexity
				for (int tries = 0; tries < creationTries; tries++) {
					newGP.create(creationType, depth, cfg.MaximumComplexity, ns);
					
					if (newGP.complexity() > cfg.MaximumComplexity) {
						//GP is too complex
						attemptedComplexCount++;
						
						//decrease depth
						if ((tries >= creationTries / 4) && (depth > minTreeDepth))
							depth--;
						
					}
					else {
						//test whether GP is unique
						if (checkForDiversity(newGP)) {
							//evaluate and store new GP
							set(i, newGP);
							break trying;
						}
						
						//increase depth if struggling to find a unique GP
						if ((tries >= creationTries / 4) && (depth < cfg.MaximumDepthForCreation))
							depth++;
					}
					
				}
				throw new RuntimeException("Unable to create valid GP in " + creationTries + " tries");
			}
			
			//increase depth for ramped schemes
			if (++treeDepth > cfg.MaximumDepthForCreation)
				treeDepth = minTreeDepth;
		}
		
		//evaluate the entire population
		calculateStatistics();
	}
	
	/**
	 * Calculates statistics about a complete population, including the best, average, and worst fitness, complexity, and
	 * depth. Also stores the indices of the best and worst individuals.
	 *
	 * @exception java.lang.RuntimeException
	 *                if a null GP is unexpectedly found in the population.
	 */
	public synchronized void calculateStatistics() {
		//search for the best, worst, and longest
		GP<C, R> worst = get(0);
		GP<C, R> best = get(0);
		worstOfPopulation = 0;
		bestOfPopulation = 0;
		
		bestFitness = best.stdFitness;
		worstFitness = bestFitness;
		
		longestComplexity = worst.complexity();
		int worstLen = longestComplexity;
		int bestLen = longestComplexity;
		
		int sumComplexity = 0;
		int sumDepth = 0;
		int sumFit = 0;
		
		for (int i = 0; i < capacity(); i++) {
			GP<C, R> current = get(i);
			if (current == null)
				throw new RuntimeException("Null GP found in population");
			
			//test for nulls anywhere in the GP
			//current.testNull();
			
			int curLen = current.complexity();
			if (curLen > longestComplexity)
				longestComplexity = curLen;
			sumComplexity += curLen;
			
			sumDepth += current.depth();
			
			double curFitness = current.stdFitness;
			//System.out.println(curFitness);
			sumFit += curFitness;
			
			if ((worstFitness < curFitness) ||
					((worstFitness == curFitness) && (worstLen < curLen))) {
				worstOfPopulation = i;
				worst = current;
				worstFitness = curFitness;
				worstLen = curLen;
			}
			
			if ((bestFitness > curFitness) ||
					((bestFitness == curFitness) && (bestLen > curLen))) {
				bestOfPopulation = i;
				best = current;
				bestFitness = curFitness;
				bestLen = curLen;
			}
		}
		
		//get averages
		avgFitness = (double) sumFit / capacity();
		avgComplexity = (double) sumComplexity / capacity();
		avgDepth = (double) sumDepth / capacity();
	}
	
	/**
	 * Converts an integer to a string right-justified in a field of specified width.
	 * 
	 * @param i
	 *            the integer to right-justify
	 * @param width
	 *            the width of the field
	 * @return a right-justified string representation with the given <tt>width</tt> of <tt>i</tt>
	 */
	protected static String formatInt(int i, int width) {
		String s = String.valueOf(i);
		while (s.length() < width)
			s = " " + s;
		return s;
	}
	
	/**
	 * @param length
	 *            the length of the overflow {@link String}
	 * @return a {@link String} of asterisks with specified <tt>length</tt>
	 */
	protected static String overflow(int length) {
		StringBuffer sb = new StringBuffer(length);
		for (int i = 1; i < length; i++)
			sb.append("*");
		return sb.toString();
	}
	
	/**
	 * Converts a double to a string right justified in a field of specified length. Returns an overflow string if the number
	 * cannot be formatted as specified. Uses scientific notation whenever {@link String#valueOf(double)} returns a number in
	 * scientific format. Rounds the number to the specified number of decimal places. Not industrial strength but works as
	 * needed for {@link Population} and {@link GPRun} reports.
	 * 
	 * @param d
	 *            the double to right-justify
	 * @param length
	 *            the length of the {@link String}
	 * @param places
	 *            the number of decimal places to display
	 * @return <tt>d</tt> formatted as specified if possible, otherwise, an overflow {@link String}
	 */
	public static String formatDouble(double d, int length, int places) {
		if (Double.isNaN(d) || Double.isInfinite(d))
			return overflow(length);
		
		String s;
		double pow10 = Math.pow(10.0, places);
		
		d = Math.rint(d * pow10) / pow10; //round to places
		if ((d < 1.0e+006) && (d > Math.pow(10.0, length - 1 - places) - Math.pow(10.0, -places)))
			//fixed-point value won't fit in field
			return overflow(length);
		
		s = String.valueOf(d);
		int ePos = s.indexOf('e');
		if (ePos < 0)
			ePos = s.indexOf('E');
		if (ePos < 0) {
			//no exponent in string
			int dpos = s.indexOf('.');
			if (dpos < 0)
				s = s + ".";						  //add decimal point
			else if (dpos + places + 1 < s.length())
				s = s.substring(0, dpos + places + 1);  //truncate places
			dpos = s.indexOf('.');
			while (s.length() - dpos < places + 1)
				s = s + "0";						  //zero-pad to places
		}
		else if (length > 6) {
			//scientific notation
			String sExp = s.substring(ePos, s.length());
			String sMant;
			if (ePos - 2 > places)
				sMant = s.substring(0, places + 2);
			else
				sMant = s.substring(0, ePos);
			s = sMant + sExp;
		}
		else
			return overflow(length);
		
		while (s.length() < length)
			s = " " + s;							  //left-pad to width
		return s;
	}
	
	/**
	 * Calls {@link #formatDouble(double, int, int)} to create a formatted {@link String} and then strips any leading blanks.
	 * 
	 * @param d
	 *            the double to right-justify
	 * @param length
	 *            the length of the {@link String}
	 * @param places
	 *            the number of decimal places to display
	 * @return <tt>d</tt> formatted as specified if possible, otherwise, an overflow {@link String}
	 * @see #formatDouble(double, int, int)
	 */
	public static String trimFormatDouble(double d, int length, int places) {
		String s = formatDouble(d, length, places);
		for (int i = 0; i < s.length(); i++)
			if (s.charAt(i) != ' ')
				return s.substring(i, s.length());
		return "";
	}
	
	/**
	 * Prints a legend line for the standard statistics report to the specified {@link PrintWriter}.
	 * 
	 * @param sout
	 *            the {@link PrintWriter} to which to print
	 */
	public void printStatisticsLegend(PrintWriter sout) {
		sout.print(" Gen|			  Fitness		   |   Complexity	|	Depth   ");
		if (cfg.TestDiversity)
			sout.print("|Variety");
		sout.println();
		sout.print("	|	  Best	Average	  Worst|   B	  A	 W|  B	A   W");
		//xxxx|xxxxxxx.xx xxxxxxx.xx xxxxxxx.xx|xxxx xxxx.x  xxxx| xx xx.x  xx| x.xxx
		if (cfg.TestDiversity)
			sout.print("|");
		sout.println();
	}
	
	/**
	 * Prints one generation's statistics to the specified PrintWriter. The output is on one line and includes the generation
	 * number; the best, average, and worst standardized fitness; the complexity of the best, average, and worst individuals;
	 * the depth of the best, average, and worst individuals; and the diversity of the population if cfg.TestDiversity is
	 * true. If this generation has been saved to a stream by {@link GPRun GPRun's} checkpointing facility, the letter 'c' is
	 * printed at the end of the line.
	 *
	 * @param generation
	 *            the generation number.
	 * @param chk
	 *            the checkpoint character ('c' or ' ').
	 * @param sout
	 *            the statistics {@link PrintWriter}
	 */
	public void printStatistics(int generation, char chk, PrintWriter sout) {
		
		sout.print(formatInt(generation, 4));
		
		sout.print("|");
		sout.print(formatDouble((get(bestOfPopulation)).stdFitness, 10, 2));
		sout.print(" ");
		sout.print(formatDouble(avgFitness, 10, 2));
		sout.print(" ");
		sout.print(formatDouble((get(worstOfPopulation)).stdFitness, 10, 2));
		
		sout.print("|");
		sout.print(formatInt((get(bestOfPopulation)).complexity(), 4));
		sout.print(" ");
		sout.print(formatDouble(avgComplexity, 6, 1));
		sout.print("  ");
		sout.print(formatInt((get(worstOfPopulation)).complexity(), 4));
		//sout.print(formatDouble(longestComplexity, 6, 1));
		
		sout.print("| ");
		sout.print(formatInt((get(bestOfPopulation)).depth(), 2));
		sout.print(" ");
		sout.print(formatDouble(avgDepth, 4, 1));
		sout.print("  ");
		sout.print(formatInt((get(worstOfPopulation)).depth(), 2));
		
		if (cfg.TestDiversity) {
			sout.print("| ");
			sout.print(formatDouble(1.0 - ((double) dupCount / capacity()), 5, 3));
		}
		
		sout.print(" ");
		sout.print(chk);
		sout.println();
	}
	
	/**
	 * Formats a {@link String} that shows the heritage of a crossover, reproduction, or mutation operation.
	 *
	 * @param len
	 *            the complexity of the {@link String} to return.
	 * @param index
	 *            the index of the parent in its {@link Population}.
	 * @param tree
	 *            the branch number in which crossover or mutation occurred. Branch 0 is converted to "RPB", branch 1 is
	 *            converted to "ADF0", and so on.
	 * @param cut
	 *            the s-expression index at which crossover or mutation occurred.
	 * @return a {@link String} that shows the heritage of the crossover, reproduction, or mutation operation
	 */
	protected String formatParentage(int len, int index, int tree, int cut) {
		
		String sIndex;
		String sTree;
		String sCut;
		
		StringBuffer sb = new StringBuffer(len);
		int pos = len;
		for (int i = 0; i < pos; i++)
			sb.append(' ');
		
		if (index >= 0) {
			sIndex = String.valueOf(index);
			pos -= sIndex.length();
		}
		else
			sIndex = "";
		
		if (tree >= 0) {
			if (tree == 0)
				sTree = "RPB";
			else
				sTree = "ADF" + (tree - 1);
			pos -= sTree.length();
		}
		else
			sTree = "";
		
		if ((index >= 0) && (tree >= 0))
			pos--;
		
		if (cut >= 0) {
			sCut = String.valueOf(cut);
			pos -= sCut.length() + 1;
		}
		else
			sCut = "";
		
		//transfer substrings to display buffer
		for (int i = 0; i < sIndex.length(); i++)
			sb.setCharAt(pos++, sIndex.charAt(i));
		if ((index >= 0) && (tree >= 0))
			sb.setCharAt(pos++, ':');
		for (int i = 0; i < sTree.length(); i++)
			sb.setCharAt(pos++, sTree.charAt(i));
		if (cut >= 0)
			sb.setCharAt(pos++, ':');
		for (int i = 0; i < sCut.length(); i++)
			sb.setCharAt(pos++, sCut.charAt(i));
		
		return sb.toString();
	}
	
	/**
	 * Prints details about a particular individual to the specified PrintWriter. The first line shows 'B' if the individual
	 * is the best in its population, or 'W' if it is the worst. The line goes on to show the population index (num) of the
	 * individual, its reproduction or crossover heritage, its mutation heritage, and its standardized fitness, complexity,
	 * and depth.
	 * <p>
	 * If showExpression is true, the GP's printOn() method is called to print the s-expression. If showTree is true, the
	 * GP's printTree() method is called to print the expression in pseudo-graphic text format.
	 * 
	 * @param current
	 *            the {@link GP} to print
	 * @param num
	 *            the index of <tt>current</tt> in the {@link Population}
	 * @param showExpression
	 *            whether to print the {@link GP GP's} s-expression
	 * @param showTree
	 *            whether to print the {@link GP} in pseudo-graphic text format via
	 *            {@link GP#printTree(PrintWriter, Configuration)}
	 * @param writer
	 *            the {@link PrintWriter} on which to print
	 */
	public void printIndividual(GP<C, R> current, int num, boolean showExpression, boolean showTree, PrintWriter writer) {
		
		if (num == bestOfPopulation)
			writer.print("B");
		else if (num == worstOfPopulation)
			writer.print("W");
		else
			writer.print(" ");
		
		writer.print(formatInt(num, 4));
		
		//display creation, copy, crossover history
		writer.print(formatParentage(14, current.dadIndex, current.crossTree, current.dadCross));
		writer.print(formatParentage(14, current.mumIndex, current.crossTree, current.mumCross));
		
		//display mutation history
		writer.print(formatParentage(9, -1, current.swapTree, current.swapPos));
		writer.print(formatParentage(9, -1, current.shrinkTree, current.shrinkPos));
		
		writer.print(" " + formatDouble(current.stdFitness, 10, 2));
		writer.print(" " + formatInt(current.complexity(), 4));
		writer.print(" " + formatInt(current.depth(), 4));
		writer.println();
		
		if (showExpression) {
			writer.println();
			current.printOn(writer, cfg);
		}
		if (showTree) {
			writer.println();
			current.printTree(writer, cfg);
		}
	}
	
	/**
	 * Prints details about some or all of the population. It starts by printing a columnar header, then calls
	 * printIndividual() for all individuals, the best, and/or the worst depending on the parameters passed to the method.
	 * 
	 * @param generation
	 *            the generation number
	 * @param showAll
	 *            whether to show all of the individuals in this {@link Population}
	 * @param showBest
	 *            whether to show the best individual in this {@link Population}
	 * @param showWorst
	 *            whether to show the worst individual in this {@link Population}
	 * @param showExpression
	 *            whether to print the s-expressions for the printed {@link GP GPs}
	 * @param showTree
	 *            whether to print the {@link GP} in pseudo-graphic text format via
	 *            {@link GP#printTree(PrintWriter, Configuration)} for the {@link GP GPs} to be printed
	 * @param writer
	 *            the {@link PrintWriter} on which to print
	 */
	public void printDetails(int generation, boolean showAll, boolean showBest, boolean showWorst, boolean showExpression, boolean showTree, PrintWriter writer) {
		if (showAll || showBest || showWorst) {
			writer.println("========================================================================");
			writer.println("Generation:" + generation + " best:" + bestOfPopulation + " worst:" + worstOfPopulation);
			writer.println("  GP#		   dad		   mum oper mut shrk mut	fitness  len  dep");
			writer.println("===== ============= ============= ======== ======== ========== ==== ====");
			//Bnnnn iiii:adf0:ppp iiii:adf0:ppp adf0:ppp adf0:ppp nnnnnnn.n llll dddd
		}
		
		GP<C, R> current;
		if (showAll)
			for (int i = 0; i < capacity(); i++) {
				current = get(i);
				printIndividual(current, i, showExpression, showTree, writer);
			}
		
		if (showBest && !showAll) {
			current = get(bestOfPopulation);
			printIndividual(current, bestOfPopulation, showExpression, showTree, writer);
		}
		
		if (showWorst && !showAll) {
			current = get(worstOfPopulation);
			printIndividual(current, worstOfPopulation, showExpression, showTree, writer);
		}
	}
	
}
