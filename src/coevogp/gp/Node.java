package coevogp.gp;

import java.io.PrintWriter;
import java.util.function.BiFunction;

import coevogp.config.Configuration;

/**
 * Holds the id, representation, number of arguments, and action for a node within a GP.<br>
 * This is used to allow faster mutation of {@link Gene genes}.
 * 
 * @author Joshua Lipstone
 */
public final class Node<C extends Configuration, R> extends GPRoot implements BiFunction<C, Gene<C, R>, R> {
	private final int id, argCount;
	private final String rep;
	private final BiFunction<C, Gene<C, R>, R> action;
	
	/**
	 * Construct a new {@link Node} with the given ID, argument count, representation, and action to perform
	 * 
	 * @param id
	 *            the id of this {@link Node}
	 * @param argCount
	 *            the number of arguments that a {@link Gene} with this {@link Node} requires
	 * @param rep
	 *            a {@link String} representation of this {@link Node} (its name is a good start)
	 * @param action
	 *            the {@link BiFunction action} that {@link Gene Genes} with this {@link Node} should perform
	 */
	public Node(int id, int argCount, String rep, BiFunction<C, Gene<C, R>, R> action) {
		this.id = id;
		this.argCount = argCount;
		this.rep = rep;
		this.action = action;
	}
	
	/**
	 * @return the id of this {@link Node}
	 */
	public final int id() {
		return id;
	}
	
	/**
	 * @return the number of arguments that this {@link Node} requires
	 */
	public final int argCount() {
		return argCount;
	}
	
	/**
	 * @return the {@link String} representation of this {@link Node}
	 */
	public final String rep() {
		return rep;
	}
	
	/**
	 * @return the {@link BiFunction action} that this {@link Node} performs
	 */
	public final BiFunction<C, Gene<C, R>, R> action() {
		return action;
	}
	
	/**
	 * A node represents a function in a GP if it requires that a {@link Gene} containing it have 1 or more arguments.
	 * 
	 * @return whether this node represents a function within a GP
	 */
	public boolean isFunction() {
		return argCount > 0;
	}
	
	public boolean isTerminal() {
		return argCount == 0;
	}
	
	@Override
	public R apply(C cfg, Gene<C, R> gene) {
		return action.apply(cfg, gene);
	}
	
	/**
	 * @return true if this {@link Node node's} id, argument count, and representation are equal to the other {@link Node
	 *         node's}
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof Node))
			return false;
		Node<?, ?> n = (Node<?, ?>) o;
		return id() == n.id() && argCount() == n.argCount() && ((rep() != null && rep().equals(n.rep())) || (rep() == null && n.rep() == null));
	}
	
	/**
	 * Writes a {@link Node} in text format to a {@link PrintWriter}. The {@link Node} is printed simply as its
	 * representation string.
	 * 
	 * @param writer
	 *            the {@link PrintWriter} on which to write
	 * @param cfg
	 *            the {@link Configuration} in use
	 * @see #rep()
	 */
	public void printOn(PrintWriter writer, C cfg) {
		writer.print(rep());
	}
	
	@Override
	public Object clone() {
		return new Node<>(this.id, this.argCount, this.rep, this.action);
	}
	
	/**
	 * Prints this {@link Node Node's} representation string.
	 * 
	 * @return {@code getRep()}
	 * @see #rep()
	 */
	@Override
	public String toString() {
		return rep();
	}
	
	@Override
	public boolean testNull() {
		return false;
	}
}
