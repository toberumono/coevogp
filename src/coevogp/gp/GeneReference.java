package coevogp.gp;

import coevogp.Container;
import coevogp.config.Configuration;

/**
 * Encapsulates the input and output parameters to the tree search routines used for crossover and mutation. This class is
 * needed because Java cannot compute the address of a variable. The functions in gpc++ that take the address of a pointer in
 * order to update a tree cannot be directly ported to Java. Instead, gpjpp maintains the container and index within the
 * container of each gene being searched. This effectively provides the address of the gene itself and makes it possible to
 * replace the gene with a gene branch from another source.
 * <p>
 * This class is for internal use.
 *
 * @version 1.0
 */
public class GeneReference<C extends Configuration, R> implements GPRoot {
	/**
	 * The container that holds a particular gene.
	 */
	public Container<Gene<C, R>> container;
	
	/**
	 * The index of a particular gene within its container.
	 */
	public int index;
	
	/**
	 * Used by a search function to count the number of genes to pass or the number that were passed.
	 */
	public int count;
	
	/**
	 * Instantiates a gene reference for later use.
	 */
	public GeneReference() {}
	
	/**
	 * Instantiates a gene reference with a known container and index.
	 * 
	 * @param container
	 *            the {@link Container} holding the {@link Gene} to reference
	 * @param index
	 *            the index of the {@link Gene} in the {@link Container}
	 */
	public GeneReference(Container<Gene<C, R>> container, int index) {
		this.container = container;
		this.index = index;
	}
	
	/**
	 * Clones another gene reference.
	 * 
	 * @param ref
	 *            the {@link GeneReference} to clone
	 */
	public GeneReference(GeneReference<C, R> ref) {
		container = ref.container;
		index = ref.index;
		count = ref.count;
	}
	
	/**
	 * Assigns another gene reference to this one.
	 * 
	 * @param ref
	 *            the {@link GeneReference} to which to assign this one
	 */
	public void assignRef(GeneReference<C, R> ref) {
		container = ref.container;
		index = ref.index;
		count = ref.count;
	}
	
	/**
	 * Assigns a particular container and index to this gene reference.
	 * 
	 * @param container
	 *            the {@link Container} holding the {@link Gene} to reference
	 * @param index
	 *            the index of the {@link Gene} in the {@link Container}
	 */
	public void assignContainer(Container<Gene<C, R>> container, int index) {
		this.container = container;
		this.index = index;
	}
	
	/**
	 * @return the {@link Gene} referenced by this {@link GeneReference}
	 */
	public Gene<C, R> getGene() {
		return container.get(index);
	}
	
	/**
	 * Overwrites the {@link Gene} referenced by this {@link GeneReference} with <tt>g</tt>
	 * 
	 * @param g
	 *            the {@link Gene} with which to overwrite the {@link Gene} that is currently being referenced
	 */
	public void setGene(Gene<C, R> g) {
		container.set(index, g);
	}
	
	@Override
	public String toString() {
		return "[" + container + ":" + index + "]";
	}
	
	@Override
	public boolean testNull() {
		return container.get(index) == null || container.testNull();
	}
	
	@Override
	public Object clone() {
		return new GeneReference<>(this);
	}
}
