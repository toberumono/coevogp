package coevogp.gp;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.function.BiFunction;

import coevogp.Container;
import coevogp.config.Configuration;

import static coevogp.Helpers.rgen;

/**
 * A set of {@link Node Nodes} that can be used by a {@link GP}.
 * 
 * @version 2.0
 * @author Joshua Lipstone
 * @param <C>
 *            the type of {@link Configuration} used by the {@link GP}
 */
public class NodeSet<C extends Configuration, R> extends Container<Node<C, R>> {
	private int numFunctions, numTerminals;
	
	/**
	 * Basic constructor
	 * 
	 * @param numNodes
	 *            the number of {@link Node nodes} in this {@link NodeSet}
	 */
	public NodeSet(int numNodes) {
		super(numNodes);
	}
	
	/**
	 * A constructor that can be called to clone a NodeSet. Normally not used.
	 * 
	 * @param gpo
	 *            the {@link NodeSet} to clone
	 */
	public NodeSet(NodeSet<C, R> gpo) {
		super(gpo);
		numFunctions = gpo.numFunctions;
		numTerminals = gpo.numTerminals;
	}
	
	/**
	 * @return the number of functions in this {@link NodeSet}
	 */
	public int numFunctions() {
		return numFunctions;
	}
	
	/**
	 * @return the number of terminals in this {@link NodeSet}
	 */
	public int numTerminals() {
		return numTerminals;
	}
	
	/**
	 * Adds the given {@link Node} to this {@link NodeSet}, expanding the size of this {@link NodeSet} if necessary.
	 * 
	 * @param node
	 *            the {@link Node} to add. This {@link Node} cannot have the same id as another {@link Node} in this
	 *            {@link NodeSet}
	 */
	public void addNode(Node<C, R> node) {
		if (getNodeByID(node.id()) != null)
			throw new RuntimeException("Cannot have duplicate node ids.");
		if (numFunctions + numTerminals == capacity())
			expand(1);
		
		if (node.isFunction())
			super.set(numFunctions++, node);
		else
			super.set(capacity() - (++numTerminals), node);
	}
	
	/**
	 * Adds the given {@link Node Nodes} to this {@link NodeSet}, expanding the size of this {@link NodeSet} if necessary.
	 * 
	 * @param nodes
	 *            a {@link Collection} of {@link Node Nodes} with unique IDs to add. None of the {@link Node Nodes} can have
	 *            the same id as another {@link Node} in this {@link NodeSet}.
	 */
	public void addNodes(Collection<Node<C, R>> nodes) {
		int delta = nodes.size() + numFunctions() + numTerminals() - capacity();
		if (delta > 0)
			expand(delta);
		for (Node<C, R> node : nodes)
			addNode(node);
	}
	
	private void expand(int delta) {
		int oc = capacity(), lim = oc - numTerminals;
		resize(capacity() + delta);
		for (int i = oc - 1; i >= lim; i--) {
			super.set(i + delta, get(i));
		}
	}
	
	public void removeByID(int id) {
		int index = indexOf(id);
		if (index < 0)
			return;
		removeByIndex(index);
	}
	
	public void removeByIndex(int index) {
		for (int i = index + 1; i < capacity(); i++)
			super.set(i, get(i + 1));
		resize(capacity() - 1);
	}
	
	/**
	 * Gets the index of the {@link Node} in the {@link NodeSet} with the given ID.
	 * 
	 * @param id
	 *            the ID of the {@link Node} of which to get the index
	 * @return the index of the {@link Node} or -1 if no {@link Node} matches <tt>id</tt>
	 */
	public int indexOf(int id) {
		for (int i = 0; i < capacity(); i++) {
			Node<C, R> node = get(i);
			if (node != null && node.id() == id)
				return i;
		}
		return -1;
	}
	
	/**
	 * Convenience method for adding {@link Node Nodes}
	 * 
	 * @param id
	 *            the id of the {@link Node} to add
	 * @param args
	 *            the number of arguments that the {@link Node} will take
	 * @param representation
	 *            the {@link String} representation of the {@link Node}
	 * @param action
	 *            a function that takes a {@link Configuration} and {@link Gene} and performs the action denoted by the
	 *            {@link Node}
	 * @see #addNode(Node)
	 */
	public void addNode(int id, int args, String representation, BiFunction<C, Gene<C, R>, R> action) {
		addNode(new Node<>(id, args, representation, action));
	}
	
	/**
	 * This method should not be called externally. Use {@link #addNode(Node)},
	 * {@link #addNode(int, int, String, BiFunction)}, {@link #addNodes(Collection)}, {@link #removeByID(int)}, or
	 * {@link #removeByIndex(int)} instead.<br>
	 * If <tt>node</tt> is null, this is equivalent to {@link #removeByIndex(int)}.
	 * 
	 * @see #addNode(Node)
	 * @see #addNode(int, int, String, BiFunction)
	 * @see #addNodes(Collection)
	 * @see #removeByID(int)
	 * @see #removeByIndex(int)
	 */
	@Override
	public Node<C, R> set(int index, Node<C, R> node) {
		Node<C, R> temp = super.set(index, node);
		if (node == null)
			removeByIndex(index);
		return temp;
	}
	
	/**
	 * Gets the first {@link Node} in this {@link NodeSet} with the given id
	 * 
	 * @param id
	 *            the id of the {@link Node} to get
	 * @return the first {@link Node} in this {@link NodeSet} with the given id or {@code null} if no such {@link Node}
	 *         exists
	 */
	public Node<C, R> getNodeByID(int id) {
		for (Node<C, R> node : this)
			if (node != null && node.id() == id)
				return node;
		return null;
	}
	
	/**
	 * Gets the first {@link Node} in this {@link NodeSet} with the given representation
	 * 
	 * @param rep
	 *            the representation of the {@link Node} to get
	 * @return the first {@link Node} in this {@link NodeSet} with the given representation or {@code null} if no such
	 *         {@link Node} exists
	 */
	public Node<C, R> getNodeByRep(String rep) {
		for (Node<C, R> node : this)
			if (node != null && node.rep().equals(rep))
				return node;
		return null;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof NodeSet))
			return false;
		NodeSet<?, ?> ns = (NodeSet<?, ?>) o;
		if (ns.numFunctions != numFunctions || ns.numTerminals != numTerminals)
			return false;
		for (int i = 0; i < numFunctions; i++)
			if (!get(i).equals(ns.get(i)))
				return false;
		for (int i = capacity() - numTerminals; i < capacity(); i++)
			if (!get(i).equals(ns.get(i)))
				return false;
		return true;
	}
	
	/**
	 * Returns a random function {@link Node} from this set.<br>
	 * Used by {@link Gene#create(coevogp.config.TreeCreationType, int, int, NodeSet)} to build new trees.
	 * 
	 * @return a random function {@link Node}
	 * @see Gene#create(coevogp.config.TreeCreationType, int, int, NodeSet)
	 * @see #chooseTerminal()
	 * @see #chooseNode()
	 */
	public Node<C, R> chooseFunction() {
		return get(rgen.nextInt(numFunctions));
	}
	
	/**
	 * Returns a random terminal {@link Node} from this set.<br>
	 * Used by {@link Gene#create(coevogp.config.TreeCreationType, int, int, NodeSet)} to build new trees.
	 * 
	 * @return a random terminal {@link Node}
	 * @see Gene#create(coevogp.config.TreeCreationType, int, int, NodeSet)
	 * @see #chooseFunction()
	 * @see #chooseNode()
	 */
	public Node<C, R> chooseTerminal() {
		//This is the unsimplified form: capacity() - (numTerminals - rgen.nextInt(numTerminals))
		return get(capacity() - numTerminals + rgen.nextInt(numTerminals));
	}
	
	/**
	 * Chooses a random {@link Node} from this set.<br>
	 * This method does <i>not</i> ensure an equal probability of getting a function or terminal {@link Node}. Each
	 * {@link Node} in this {@link NodeSet} has an equal probability of being returned.<br>
	 * 
	 * @return a random function or terminal {@link Node}
	 * @see #chooseFunction()
	 * @see #chooseTerminal()
	 */
	public Node<C, R> chooseNode() {
		int node = rgen.nextInt(numFunctions + numTerminals);
		if (node > numFunctions) //Then it is a terminal
			//Terminals start at capacity() - 1, so we have to use an offset.
			//We add numFunctions because technically we are subtracting (node - numFunctions) - this just simplifies the equation a bit.
			return get(capacity() - node + numFunctions);
		return get(node); //Functions start at 0, so if it is a function, we don't need to use any offset
	}
	
	/**
	 * Writes a {@link NodeSet} in text format to a {@link PrintWriter}. Each {@link Node} is printed as its representation
	 * string followed by its number of arguments, if any.
	 * 
	 * @param writer
	 *            the {@link PrintWriter} on which to write
	 * @param cfg
	 *            the {@link Configuration} in use
	 */
	public void printOn(PrintWriter writer, C cfg) {
		for (int i = 0; i < capacity(); i++) {
			Node<C, R> current = get(i);
			if (i > 0)
				writer.print(" ");
			if (current != null) {
				current.printOn(writer, cfg);
				if (current.isFunction())
					writer.print("(" + current.argCount() + ")");
			}
			else
				writer.print("null");
		}
	}
}
