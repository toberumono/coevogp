package coevogp.gp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import coevogp.Container;
import coevogp.config.Configuration;
import coevogp.config.TreeCreationType;

import static coevogp.Helpers.rgen;

public class Gene<C extends Configuration, R> extends Container<Gene<C, R>> {
	private Node<C, R> node;
	
	/**
	 * Creates a {@link Gene} with the given {@link Node}, but does not populate it.
	 * 
	 * @param node
	 *            the {@link Node} of the new {@link Gene}
	 */
	public Gene(Node<C, R> node) {
		super(node.argCount());
		this.node = node;
	}
	
	/**
	 * Constructs a new {@link Gene} that is a deep-copy of <tt>original</tt> (the entire tree is duplicated).<br>
	 * This is the copy constructor for {@link Gene}.
	 * 
	 * @param original
	 *            the original {@link Gene}
	 */
	Gene(Gene<C, R> original) {
		super(original);
		this.node = original.node;
	}
	
	@Override
	public Object clone() {
		return new Gene<>(this);
	}
	
	/**
	 * Called by the game to determine what move a Player using this GP will make
	 * 
	 * @param cfg
	 *            the configuration for the current run
	 * @return an integer representing the move to take
	 */
	public R evaluate(C cfg) {
		return node.apply(cfg, this);
	}
	
	/**
	 * Subclasses must override this method in order for the creation function to work properly
	 * 
	 * @param node
	 *            the node to use for the child {@link Gene}
	 * @return a {@link Gene} or a subclass thereof with the given node and an allocated argument capacity that has not been
	 *         filled.
	 */
	protected Gene<C, R> createChild(Node<C, R> node) {
		return new Gene<>(node);
	}
	
	/**
	 * @return true if this {@link Gene} represents a function {@link Node}
	 */
	public boolean isFunction() {
		return capacity() > 0;
	}
	
	/**
	 * @return true if this {@link Gene} represents a terminal {@link Node}
	 */
	public boolean isTerminal() {
		return capacity() == 0;
	}
	
	/**
	 * @return the {@link Node} reference of this {@link Gene}
	 */
	public Node<C, R> getNode() {
		return node;
	}
	
	/**
	 * Changes the {@link Node} associated with this {@link Gene} to <tt>newNode</tt>.<br>
	 * Node: This does <em>not</em> reallocate the container to comply with the new argument count.
	 * 
	 * @param newNode
	 *            the new {@link Node} to associate with this {@link Gene}
	 */
	public void setNode(Node<C, R> newNode) {
		this.node = newNode;
	}
	
	/**
	 * Returns the string representation of this gene as given by its node type. This method can be overridden in a user
	 * subclass of {@link Gene} if the string representation should vary depending on the data value of each gene.
	 * 
	 * @return the {@link String} representation of this {@link Gene}
	 * @see Node#rep()
	 */
	public String getRep() {
		return node.rep();
	}
	
	/**
	 * @return the number of arguments that this {@link Gene Gene's} {@link Node} requires
	 * @see Node#argCount()
	 */
	public int getArgCount() {
		return node.argCount();
	}
	
	/**
	 * Returns the total number of function genes included by this gene and all of its children. If this gene is a terminal,
	 * countFunctions returns 0. Otherwise it returns at least 1 and recursively traces all of its children. Used internally
	 * during shrink mutation.
	 * 
	 * @return the number of function {@link Gene Genes} within this {@link Gene Gene's} subtree, including itself
	 */
	protected int countFunctions() {
		if (!isFunction())
			return 0;
		return 1 + stream().reduce(0, (a, b) -> a + b.countFunctions(), (a, b) -> a + b);
	}
	
	/**
	 * @return the number of genes attached to this one, including itself (the complexity of the branch) (if this
	 *         {@link Gene} is a terminal, this returns 1)
	 */
	public int complexity() {
		int complexity = 1;
		for (Gene<C, R> g : this)
			complexity += g.complexity();
		return complexity;
	}
	
	/**
	 * @return the largest depth of the tree attached to this {@link Gene}, including itself (if this {@link Gene} is a
	 *         terminal, this returns 1)
	 * @see #isTerminal()
	 */
	public int depth() {
		int max = 0;
		for (Gene<C, R> g : this) {
			int td = g.depth();
			if (td > max)
				max = td;
		}
		return 1 + max;
	}
	
	/**
	 * Creates the arguments to this gene and recursively creates children according to the limits and methods specified. The
	 * gene for which this method is called should have been created by calling the
	 * {@link GP#create(TreeCreationType, int, int, NodeSet)} method of GP, which allocates an argument container of
	 * appropriate size and assigns the node field but doesn't fill in the children. create() is called by
	 * {@link GP#create(TreeCreationType, int, int, NodeSet)}.
	 *
	 * @param creationType
	 *            the method used to create the tree, either {@link TreeCreationType#Grow} (use function nodes to fill the
	 *            tree to allowable depth) or {@link TreeCreationType#Variable} (choose function and terminal nodes with
	 *            50:50 probability).
	 * @param allowableDepth
	 *            the maximum allowable depth of the tree starting from this level. If the allowable depth is 1, the children
	 *            are always chosen to be terminals.
	 * @param allowableLength
	 *            the maximum allowable number of nodes in the tree starting at this level. Since create() cannot predict how
	 *            many nodes will be added recursively it simply stops adding nodes if it exceeds allowableLength. A higher
	 *            level routine in GPPopulation rejects the returned tree if the GP complexity exceeds a global limit.
	 * @param ns
	 *            the node set used to select functions and terminals for this branch of the GP tree.
	 * @return the total number of nodes in the created tree. If this value exceeds allowableLength, the tree will be
	 *         rejected. create() ensures that it doesn't waste much time creating extra nodes.
	 */
	public int create(TreeCreationType creationType, int allowableDepth, int allowableLength, NodeSet<C, R> ns) {
		int length = 1;
		for (int i = 0; i < capacity(); i++) {
			//decide whether to create a function or terminal for this argument
			boolean chooseTerm;
			if (allowableDepth <= 1)
				//no more depth, must use terminal
				chooseTerm = true;
			else if (creationType == TreeCreationType.Grow)
				//use functions to allowableDepth
				chooseTerm = false;
			else
				//50:50 chance of choosing a function or a terminal
				chooseTerm = rgen.flip(50.0);
			
			//create a new gene of chosen type and add it
			Gene<C, R> g = createChild(chooseTerm ? ns.chooseTerminal() : ns.chooseFunction());
			set(i, g);
			
			//if function node, call recursively to allowed depth
			if (chooseTerm)
				length++;
			else
				length += g.create(creationType, allowableDepth - 1, allowableLength - length, ns);
			
			//stop early if complexity limit exceeded
			if (length > allowableLength)
				break;
		}
		return length;
	}
	
	/**
	 * Determines whether this {@link Gene} equals another object. It returns true if obj is not null, is an instance of a
	 * {@link Gene} (or a descendant), and has the same structure and node values as this gene. This function is called when
	 * a Population is testing the diversity of the population.
	 * <p>
	 * You might need to override this in cases where two terminal genes can have identical node types but still not be the
	 * same. This occurs for node types that represent random constants, for example.
	 *
	 * @param obj
	 *            any Java object reference, including null.
	 * @return true if this and obj are equivalent.
	 */
	@Override
	public boolean equals(Object obj) {
		if ((obj == null) || !(obj instanceof Gene<?, ?>))
			return false;
		Gene<?, ?> g = (Gene<?, ?>) obj;
		if (node != g.getNode() || capacity() != g.capacity())
			return false;
		for (int i = 0; i < capacity(); i++)
			if ((get(i) != null && !get(i).equals(g.get(i))) || (get(i) == null && g.get(i) != null))
				return false;
		return true;
	}
	
	/**
	 * Returns true if a gene of specified position and type can be found within this gene. Called within
	 * {@link Gene#chooseFunctionOrTerminalNode(GeneReference)} and {@link Gene#chooseFunctionNode(GeneReference)} of this
	 * class, which are used to select appropriate genes for crossover and mutation.
	 *
	 * @param ref
	 *            on entry specifies the container that holds this gene, the index of this gene within that container, and
	 *            the count of genes to scan. On exit returns the container that holds the found gene and the index of that
	 *            gene within the container.
	 * @param findFunction
	 *            true if only function nodes are to be counted, false if function and terminal nodes are acceptable.
	 * @return true if an acceptable node is found; false if there are fewer nodes than specified.
	 */
	protected boolean findNthNode(GeneReference<C, R> ref, boolean findFunction) {
		//return ref when count nodes of proper type have been visited
		if (!findFunction || (capacity() > 0))
			if (--ref.count <= 0)
				return true;
		
		//otherwise scan children of this node
		for (int i = 0; i < capacity(); i++) {
			ref.assignContainer(this, i);
			if (get(i).findNthNode(ref, findFunction))
				return true;
		}
		
		//proper node not found
		return false;
	}
	
	/**
	 * Attempts to find a random function node within this gene, not considering this gene itself. If 10 random attempts
	 * don't find a function, it returns a terminal as a last resort. Used for {@link GP#cross(GP, int, int)} and
	 * {@link GP#swapMutation(Configuration, NodeSet)} by the GP class.
	 *
	 * @param ref
	 *            on entry specifies the container that holds this gene and the index of this gene within that container. On
	 *            exit returns the container that holds the found gene, the index of that gene within the container, and the
	 *            number of genes counted to reach the found one.
	 * @exception RuntimeException
	 *                if findNthNode can't find a node that should rightfully exist in the tree.
	 */
	public void chooseFunctionOrTerminalNode(GeneReference<C, R> ref) {
		//calculate the length of the subtree
		int totalLength = complexity();
		
		//loop trying to return a function
		Container<Gene<C, R>> saveContainer = ref.container;
		int saveIndex = ref.index;
		int maxTries = 10;
		for (int i = 0; i < (totalLength < maxTries ? totalLength : maxTries); i++) {
			//restore starting ref
			ref.assignContainer(saveContainer, saveIndex);
			
			//count a random distance into subtree
			int saveCount = 1 + rgen.nextInt(totalLength);
			ref.count = saveCount;
			
			if (!findNthNode(ref, false))
				throw new RuntimeException("Couldn't find expected tree node");
			
			//return count in gene reference
			ref.count = saveCount;
			if (ref.getGene().isFunction())
				return;
		}
		//otherwise return a terminal
	}
	
	/**
	 * Finds a random function node within this gene, not considering this gene itself. If no such function exists, the
	 * function returns false. Used for {@link GP#shrinkMutation()} by the GP class.
	 *
	 * @param ref
	 *            on entry specifies the container that holds this gene and the index of this gene within that container. On
	 *            exit returns the container that holds the found gene, the index of that gene within the container, and the
	 *            number of genes counted to reach the found one.
	 * @return true if any function node can be found, else false.
	 */
	public boolean chooseFunctionNode(GeneReference<C, R> ref) {
		//choose a random number in the range of available functions
		//exclude this gene
		int totalFunctions = countFunctions();
		if (totalFunctions > 0) {
			int saveCount = 1 + rgen.nextInt(totalFunctions);
			ref.count = saveCount;
			if (findNthNode(ref, true)) {
				//return count for mutation tracking
				ref.count = saveCount;
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	
	/**
	 * Writes this {@link Gene} in text format to a {@link PrintWriter}. Each node is printed as its representation string
	 * followed by its children, if any, in recursive depth-first order. Parentheses are used to surround the arguments to
	 * each function. The entire expression is written on a single text line.
	 * 
	 * @param writer
	 *            the {@link PrintWriter} onto which to print this {@link Gene}
	 * @param cfg
	 *            the {@link Configuration} instance being used by the caller
	 */
	public void printOn(PrintWriter writer, C cfg) {
		if (isFunction())
			writer.print("(");
		
		writer.print(getRep());
		
		//print all children
		for (int i = 0; i < capacity(); i++) {
			writer.print(" ");
			get(i).printOn(writer, cfg);
		}
		
		if (isFunction())
			writer.print(")");
	}
	
	/**
	 * Writes a {@link Gene} in text tree format to a PrintWriter. The <a href="gpjpp.GPGenePrint.html#_top_">GPGenePrint</a>
	 * class is used to format the tree in a pseudo-graphic format. Each node is printed as its representation string and is
	 * connected to its children on a lower text row using line drawing characters.
	 *
	 * @param writer
	 *            a PrintWriter.
	 * @param cfg
	 *            the set of gpjpp configuration variables, sometimes used to control the output.
	 */
	public void printTree(PrintWriter writer, C cfg) {
		(new GenePrint<>(this)).printOn(writer, cfg);
	}
	
	/**
	 * Writes a {@link Gene} in graphic gif file format. The <a href="gpjpp.GPGenePrint.html#_top_">GPGenePrint</a> class is
	 * used to format the tree, which is drawn onto the AWT-based offscreen drawing surface represented by GPDrawing. This
	 * offscreen drawing is then encoded into the gif format and stored in the file named by fname.
	 *
	 * @param ods
	 *            an instantiated drawing surface. GPDrawing is a subclass of java.awt.Frame containing a single Canvas
	 *            component whose image is dynamically sized to hold the tree.
	 * @param fname
	 *            the name of the file to hold the gif image. This name should include the .gif extension.
	 * @param title
	 *            a string that is drawn on the first line of the image to title it. Can be null or empty.
	 * @param cfg
	 *            configuration parameters for the genetic run. The <a href="gpjpp.GPVariables.html#TreeFontSize">
	 *            TreeFontSize</a> field is used to determine the font size for text in the drawing.
	 * @exception java.io.IOException
	 *                if an error occurs while writing the image file.
	 */
	public void drawOn(Drawing ods, String fname, String title, C cfg) throws IOException {
		(new GenePrint<>(this)).drawOn(ods, fname, title, cfg);
	}
	
	public Stream<Gene<C, R>> stream() {
		return StreamSupport.stream(spliterator(), false);
	}
	
	@Override
	public String toString() {
		StringBuilder out = new StringBuilder(capacity() * 2);
		out.append("(");
		out.append(node.toString());
		for (Gene<C, R> g : this)
			out.append(" ").append(g.toString());
		out.append(")");
		return out.toString();
	}
}
