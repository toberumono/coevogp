package coevogp;

import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class Helpers {
	public static final RandomCoin rgen = new RandomCoin();
	
	/**
	 * Use this for all threading operations.
	 */
	public static final ForkJoinPool fjPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors(), ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
	
	private Helpers() {/* Makes this class static */}
	
	/**
	 * Randomly rearranges the values in the given section of the given array
	 * 
	 * @param start
	 *            the index of the start of the section
	 * @param end
	 *            the index of the end of the section
	 * @param array
	 *            the array on which to work
	 */
	public static void scrambleInRange(int start, int end, Object[] array) {
		for (; start < end; start++) {
			int swap = rgen.nextInt(start, end);
			Object temp = array[start];
			array[start] = array[swap];
			array[swap] = temp;
		}
	}
	
	/**
	 * Randomly rearranges the values in the given section of the given array
	 * 
	 * @param start
	 *            the index of the start of the section
	 * @param end
	 *            the index of the end of the section
	 * @param list
	 *            the array on which to work
	 * @param <T>
	 *            the type of the elements in <tt>list</tt>
	 */
	public static <T> void scrambleInRange(int start, int end, List<T> list) {
		for (; start < end; start++) {
			int swap = rgen.nextInt(start, end);
			T temp = list.get(start);
			list.set(start, list.get(swap));
			list.set(swap, temp);
		}
	}
}
