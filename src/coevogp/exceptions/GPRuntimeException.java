package coevogp.exceptions;

public abstract class GPRuntimeException extends RuntimeException {
	
	public GPRuntimeException(String message) {
		super(message);
	}
}
