package coevogp.exceptions;

public abstract class GPException extends Exception {

	public GPException(String message) {
		super(message);
	}
}
