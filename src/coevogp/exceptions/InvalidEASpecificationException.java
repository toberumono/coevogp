package coevogp.exceptions;

/**
 * Thrown when the parameters that determine the evolution algorithm do not resolve to a valid algorithm.
 * 
 * @author Joshua Lipstone
 * @see coevogp.config.EvolutionType
 * @see coevogp.config.SelectionType
 */
public class InvalidEASpecificationException extends GPRuntimeException {
	
	public InvalidEASpecificationException(String message) {
		super(message);
	}
}
