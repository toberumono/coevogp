package coevogp;

import java.util.Arrays;
import java.util.Iterator;

import coevogp.gp.GPRoot;

public class Container<T extends GPRoot> implements GPRoot, Iterable<T> {
	private GPRoot[] back;
	
	public Container(int capacity) {
		back = new GPRoot[capacity];
	}
	
	@SuppressWarnings("unchecked")
	public Container(Container<T> original) {
		back = new GPRoot[original.capacity()];
		for (int i = 0; i < capacity(); i++) {
			GPRoot current = original.back[i];
			back[i] = (current == null ? null : (T) current.clone());
		}
	}
	
	/**
	 * @param index
	 *            the index of the element to get
	 * @return the element at the given index
	 */
	public T get(int index) {
		return elementRead(index);
	}
	
	@SuppressWarnings("unchecked")
	protected T elementRead(int index) {
		return (T) back[index];
	}
	
	/**
	 * @param index
	 *            the index to which to write <tt>element</tt>
	 * @param element
	 *            the value to write
	 * @return the value previously at <tt>index</tt>
	 */
	public T set(int index, T element) {
		return elementWrite(index, element);
	}
	
	@SuppressWarnings("unchecked")
	protected T elementWrite(int index, T element) {
		T old = (T) back[index];
		back[index] = element;
		return old;
	}
	
	/**
	 * @return the maximum capacity of this {@link Container}
	 */
	public int capacity() {
		return back.length;
	}
	
	/**
	 * Sets all container elements to null to ensure that no references remain to objects currently held by this container.
	 * This was used during testing of garbage collection but is not normally called by gpevo.
	 */
	public void clear() {
		for (int i = 0; i < capacity(); i++)
			back[i] = null;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof Container))
			return false;
		Container<?> c = (Container<?>) o;
		if (c.capacity() != capacity())
			return false;
		for (int i = 0; i < capacity(); i++) {
			T t = get(i);
			if ((t != null && !t.equals(c.get(i))) || (t == null && c.get(i) != t))
				return false;
		}
		return true;
	}
	
	/**
	 * Resizes this {@link Container} by either padding it with nulls or dropping elements
	 * 
	 * @param newSize
	 *            the new size for this {@link Container}
	 */
	public void resize(int newSize) {
		back = Arrays.copyOf(back, newSize);
	}
	
	/**
	 * @return the <tt>array</tt> that backs this {@link Container} as an array of {@link GPRoot}
	 */
	public GPRoot[] getBacking() {
		return back;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < capacity();
			}
			
			@Override
			public T next() {
				return elementRead(i++);
			}
		};
	}
	
	@Override
	public Object clone() {
		return new Container<>(this);
	}
	
	/**
	 * Recursively tests whether any of the values stored in this {@link Container} are <tt>null</tt> or contain a
	 * <tt>null</tt> value.
	 * 
	 * @return true if there is a <tt>null</tt> somewhere within this {@link Container} or any of the items it contains
	 */
	@Override
	public boolean testNull() {
		for (GPRoot r : this)
			if (r == null || r.testNull())
				return true;
		return false;
	}
}
