package coevogp;

import java.util.function.Function;

import coevogp.config.Configuration;
import coevogp.gp.Gene;

/**
 * Represents a game that can be played to determine the fitness of some number of {@link coevogp.gp.GP GPs}.
 * 
 * @author Joshua Lipstone
 * @param <C>
 *            the type of {@link Configuration}
 * @param <Pl>
 *            the type of {@link Player}
 */
public interface Game<C extends Configuration, Pl extends Player<C>, R> {
	
	/**
	 * Plays a round of this {@link Game} and returns the resulting fitnesses of the <tt>players</tt>.
	 * 
	 * @param cfg
	 *            the {@link Configuration} data
	 * @param players
	 *            the {@link Player players} that will be playing a round of this {@link Game}
	 * @return the fitnesses of the {@link Player players}
	 */
	public double[] play(C cfg, Pl[] players);
	
	/**
	 * Plays a round of this {@link Game} with the given <tt>player</tt> against random players.
	 * 
	 * @param cfg
	 *            the {@link Configuration} data
	 * @param player
	 *            the {@link Player player} that will be playing a round of this {@link Game} against the random players
	 * @return the fitness of the given {@link Player}
	 */
	public double playRandom(C cfg, Pl player);
	
	/**
	 * Same as playRandom, but returns 1 if <tt>player</tt> wins, otherwise 0
	 * 
	 * @param cfg
	 *            the {@link Configuration} data
	 * @param player
	 *            the {@link Player player} that will be playing a round of this {@link Game} against the random players
	 * @return 1 if <tt>player</tt> wins, otherwise 0
	 */
	public int playRandomWon(C cfg, Pl player);
	
	/**
	 * Constructs a {@link Player} with the given number and <tt>gene</tt> for its logic-system
	 * 
	 * @param number
	 *            the player number
	 * @param gene
	 *            the {@link Gene} that provides the logic for this player
	 * @return a new {@link Player} that uses <tt>gene</tt> for its logic
	 */
	public Pl makePlayer(int number, final Gene<C, R> gene);
	
	/**
	 * Constructs a {@link Player} with the given number and <tt>move</tt> for its logic-system
	 * 
	 * @param number
	 *            the player number
	 * @param move
	 *            the function that this {@link Player} uses for its logic
	 * @return a new {@link Player} that uses <tt>move</tt> for its logic
	 */
	public Pl makePlayer(int number, Function<C, R> move);
	
	/**
	 * Due to how Java handles generic arrays, this is the only realistic way to get an array of {@link Player
	 * Players}
	 * 
	 * @param length
	 *            the length of the array
	 * @return an empty {@link Player} array with the provided length
	 */
	public Pl[] makeArray(int length);
}
