package coevogp;

import coevogp.config.Configuration;

/**
 * Represents a player in a {@link Game}.
 * 
 * @author Joshua Lipstone
 * @param <C>
 *            the subclass of {@link Configuration} to be used
 */
public abstract class Player<C extends Configuration> {
	private final int number;
	
	public Player(int number) {
		this.number = number;
	}
	
	/**
	 * @return the player number of this {@link Player}
	 */
	public final int getNumber() {
		return number;
	}
	
	public abstract void makeMove(C cfg);
}
