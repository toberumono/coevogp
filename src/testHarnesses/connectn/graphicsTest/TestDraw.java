package testHarnesses.connectn.graphicsTest;

import testHarnesses.connectn.game.CNConfiguration;
import testHarnesses.connectn.game.CNGame;
import testHarnesses.connectn.game.CNPlayer;
import testHarnesses.connectn.game.ConnectN;

//This class takes care of all graphics for the connect four game. It supports 
// human player v. evolved player and evolved player v. random player
public class TestDraw {
    private int w, h, xCenter;     // width and height of the board
    private static final int size = 80; // number of pixels per side of a grid square
    private CNConfiguration cfg; //access the actaul game
    
    // construct a window and board
    public TestDraw(CNConfiguration cfg) {
		w = cfg.BoardWidth;
		h = cfg.BoardHeight;
		//System.out.println(w+", "+h);
		xCenter=(w*size+200)/2;
		this.cfg = cfg;
		
		init();	//this just makes the window
		reset(); //this actually draws the background board
    }

    // simply construct a window of the right size, and set GUI hooks
    private void init() {
		Draw.initGraphicsWindow(w*size+200, h*size+80, "Board" );
		Draw.setPenRadius(.004);

		cfg.board.setHook(this::addPiece);
		cfg.board.setHookClear(this::reset);
	}

	//Not currently being used. This is the basic grid design
	public void resetBasic(){
		for (int i=1; i<w+1; i++){
			for (int j=1; j<h+1; j++) {
				Draw.square(size*i+size/2,size*j+size/2-40, size/2);
			}
		}
	}

	//This function draws the board with a gray background, yellow lattice, and blue borders like the real thing.
	//It works for most reasonable board dimensions
	 private void reset() {
		Draw.clear(Draw.LIGHT_GRAY);
		Draw.setPenColor(Draw.YELLOW);
		Draw.filledRectangle(w*size/2+120-size/2,h*size/2+40,w*size/2+20,h*size/2+10);
		//draw the circles
		Draw.setPenColor(Draw.LIGHT_GRAY);
		for (int i=1; i<w+1; i++){
			for (int j=1; j<h+1; j++) {
				Draw.filledCircle(size*i+size/2,size*j+size/2-40, size/2.3);
			}
		}
		//draw blue borders
		Draw.setPenColor(Draw.BLUE);
		Draw.filledRectangle(55,h*size/2+55,10,h*size/2+25);
		Draw.filledRectangle(w*size+105,h*size/2+55,10,h*size/2+25);
		Draw.filledRectangle(w*size/2+80,h*size+55,w*size/2+30,10);
	}

	//Draws either a red or black piece at the correct location
	private void addPiece(int col, int row, int player){
		if (player==1){
			Draw.setPenColor(Draw.RED);
			Draw.filledCircle(size*(col+1)+size/2,size*(row+1)+size/2-40,size/2.2);
			//System.out.println(size*(row+1)+size/2-40);
		}
		else if (player==2){
			Draw.setPenColor(Draw.BLACK);
			Draw.filledCircle(size*(col+1)+size/2,size*(row+1)+size/2-40,size/2.2);
		}
	}
	
	//not working right now
	private void declareWinner(int winner){
		if (winner==1){
			Draw.text(xCenter-10, 10, "You win. Humans have hope after all!");
		}
		else if (winner==-1){
			Draw.text(xCenter-10, 10, "Computer wins. Robot takeover is inevitable.");
		}
	}

	//Main runs a human player vs. an evolved player. First it evolves the player and then 
	//takes input from the human to play a game. To select a move, just type the number for the column
	//you want. You don't go through the terminal, just through the graphics winow.
    public static void main(String[] args) {
    	ConnectN t = new ConnectN("ConnectN");
    	//evolve a population
    	t.run();
    	TestDraw test = new TestDraw(t.getCFG());
    	
    	//human player is p1
    	CNPlayer p1 = CNPlayer.makePlayer("Human", 1, cfg -> {
    		int col = 0;

        		//System.out.print("Please enter the column: ");
				boolean moveDone = false;
        		while (! moveDone){
        			moveDone=false;
        		if(Draw.hasNextKeyTyped()){
        			char temp = Draw.nextKeyTyped();
        			
        			try {
        				col = Character.getNumericValue(temp);
        				moveDone = true;
        				break;
        			}
        			catch (NumberFormatException e) {
        				System.out.println("Invalid column.");
        			}
        		}

    		}
    	
        	if (col==0){
        		col=cfg.BoardWidth;
        	}
    		return col-1;
    	});
    	
    	//get the evolved player (p2)
    	CNPlayer p2 = CNPlayer.makePlayer("Evolved", 2, t.getBest().get(0)::evaluate);
    	CNGame game = new CNGame();
    	test.declareWinner(-1);

    	game.play(t.getCFG(), new CNPlayer[]{p1, p2});
    	test.declareWinner(1);

    	int winner = t.getCFG().board.isGameOver(1);
    	test.declareWinner(winner);
    	
    }

}