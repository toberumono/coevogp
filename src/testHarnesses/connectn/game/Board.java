package testHarnesses.connectn.game;

import coevogp.Player;

import static coevogp.Helpers.rgen;

public class Board {
	private final int[][] board;
	private final int maxPieces, width, height, pieceDisplayWidth;
	private int totalPieces;
	private final int[] pieces;
	private final int connectNum;
	private GUIHook hook;
	private GUIHook2 hook2;
	private int lastColumn;
	
	public Board(CNConfiguration cfg, int numPlayers) {
		width = cfg.BoardWidth;
		height = cfg.BoardHeight;
		maxPieces = width * height;
		board = new int[width][height];
		pieces = new int[numPlayers];
		pieceDisplayWidth = pieces.length / 10 + 1;
		connectNum = cfg.ConnectNum;
		this.setHook(null);
		this.setHookClear(null);
	}
	
	public Board(Board original) {
		width = original.width;
		height = original.height;
		maxPieces = original.maxPieces;
		board = new int[width][height];
		pieces = new int[original.pieces.length];
		pieceDisplayWidth = original.pieceDisplayWidth;
		connectNum = original.connectNum;
		this.setHook(null);
		this.setHookClear(null);
	}
	
	public Board(CNConfiguration cfg, int numPlayers, GUIHook hook) {
		this(cfg, numPlayers);
		this.setHook(hook);
		this.setHookClear(hook2);
	}
	
	/**
	 * @return the current GUI hook for making a move
	 */
	public GUIHook getHook() {
		return hook;
	}
	
	/**
	 * @param hook
	 *            the hook to set
	 */
	public void setHook(GUIHook hook) {
		this.hook = hook;
	}
	
	/**
	 * @return the current GUI hook for clearing board
	 */
	public GUIHook2 getHookClear() {
		return hook2;
	}
	
	/**
	 * @param hook2
	 *            the hook to set
	 */
	public void setHookClear(GUIHook2 hook2) {
		this.hook2 = hook2;
	}
	
	public void reset() {
		
		for (int i = 0; i < pieces.length; i++)
			pieces[i] = 0;
		totalPieces = 0;
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				board[i][j] = 0;
		
		if (getHookClear() != null)
			getHookClear().clear();
	}
	
	/**
	 * Inserts a piece for the given <tt>player</tt> in the given <tt>column</tt> if the move is valid.
	 * 
	 * @param player
	 *            the player number for which to insert the piece
	 * @param column
	 *            the column in which to attempt to insert the piece, [1, boardWidth]
	 * @return true if the piece was successfully inserted, otherwise false (effectively, return false if the column was
	 *         full)
	 */
	public boolean insertPiece(int player, int column) {
		int i = 0;
		//column--;
		column %= width;
		for (; i < board[column].length && board[column][i] == 0; i++); //Step through the column until we either hit the bottom or a slot with a piece in it.
		if (--i < 0) //Step back one in order to get the insertion index.  If there was a piece in the top slot, return false.
			return false;
		board[column][i] = player;
		totalPieces++;
		pieces[player - 1]++;
		if (getHook() != null)
			getHook().update(column, i, player);
		
		lastColumn = column;
		return true;
	}
	
	/**
	 * @param player
	 *            the player number for which to get the number of pieces
	 * @return the number of pieces that have been placed by the given player
	 */
	public int getNumPieces(int player) {
		return pieces[player - 1];
	}
	
	/**
	 * Determines the maximum number of adjacent pieces in a row, column, or diagonal for each player.
	 * 
	 * @return an array of the largest number of adjacent pieces for each player. Access with
	 *         {@code getMaxAdjacentPieces()[playerNumber - 1]}
	 */
	public int[] getMaxAdjacentPieces() {
		int[] max = new int[pieces.length];
		int lastPlayer = 0, adj = 0;
		//Rows
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (board[j][i] != lastPlayer) {
					if (lastPlayer > 0 && adj > max[lastPlayer - 1])
						max[lastPlayer - 1] = adj;
					adj = 0;
					lastPlayer = board[j][i];
				}
				adj++;
			}
			if (lastPlayer > 0 && adj > max[lastPlayer - 1])
				max[lastPlayer - 1] = adj;
			lastPlayer = 0;
		}
		
		//Cols
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (board[i][j] != lastPlayer) {
					if (lastPlayer > 0 && adj > max[lastPlayer - 1])
						max[lastPlayer - 1] = adj;
					adj = 0;
					lastPlayer = board[i][j];
				}
				adj++;
			}
			if (lastPlayer > 0 && adj > max[lastPlayer - 1])
				max[lastPlayer - 1] = adj;
			lastPlayer = 0;
		}
		
		//Diagonals
		//Down + left
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; i + k < width && j + k < height; k++) {
					if (board[i + k][j + k] != lastPlayer) {
						if (lastPlayer > 0 && adj > max[lastPlayer - 1])
							max[lastPlayer - 1] = adj;
						adj = 0;
						lastPlayer = board[i + k][j + k];
					}
					adj++;
				}
				if (lastPlayer > 0 && adj > max[lastPlayer - 1])
					max[lastPlayer - 1] = adj;
				lastPlayer = 0;
			}
		}
		//Up + right
		for (int i = 0; i < width; i++) {
			for (int j = height - 1; j >= 0; j--) {
				for (int k = 0; i + k < width && j - k >= 0; k++) {
					if (board[i + k][j - k] != lastPlayer) {
						if (lastPlayer > 0 && adj > max[lastPlayer - 1])
							max[lastPlayer - 1] = adj;
						adj = 0;
						lastPlayer = board[i + k][j - k];
					}
					adj++;
				}
				if (lastPlayer > 0 && adj > max[lastPlayer - 1])
					max[lastPlayer - 1] = adj;
				lastPlayer = 0;
			}
		}
		return max;
	}
	
	/**
	 * Determines the maximum number of adjacent pieces in a row, column, or diagonal for a single player.
	 * 
	 * @param player
	 *            the player number ({@link Player#getNumber()}) for which to determine the maximum number of adjacent pieces
	 * @return returns the highest number of adjacent pieces for the <tt>player</tt>
	 */
	public int getMaxAdjacentPieces(int player) {
		if (player <= 0)
			return 0;
		return getMaxAdjacentPieces()[player - 1];
	}
	
	/**
	 * @param player
	 *            the player number for which to get the number of pieces on edges
	 * @return number of edge pieces that belong to <tt>player</tt>
	 */
	public int getEdges(int player) {
		int count = 0;
		//top and bottom
		for (int i = 0; i < width; i++) {
			if (board[i][0] == player)
				count++;
			if (board[i][height - 1] == player)
				count++;
		}
		//left and right
		for (int j = 0; j < height; j++) {
			if (board[0][j] == player)
				count++;
			if (board[width - 1][j] == player)
				count++;
		}
		return count;
	}
	
	/**
	 * @param player
	 *            the player number for which to get the number of pieces near, but not on, edges
	 * @return returns number of pieces near, but not on, edges that belong to <tt>player</tt>
	 */
	public int getNearEdges(int player) {
		int count = 0;
		//top and bottom
		for (int i = 1; i < width; i++) {
			if (board[i][1] == player)
				count++;
			if (board[i][height - 2] == player)
				count++;
		}
		//left and right
		for (int j = 1; j < height; j++) {
			if (board[1][j] == player)
				count++;
			if (board[width - 2][j] == player)
				count++;
		}
		return count;
	}
	
	/**
	 * @param player
	 *            the player number for which to get the number of pieces on corners
	 * @return number of corner pieces belonging to <tt>player</tt>
	 */
	public int getCorners(int player) {
		int count = 0;
		if (board[0][0] == player)
			count++; //top left
		if (board[0][height - 1] == player)
			count++; //bottom left
		if (board[width - 1][height - 1] == player)
			count++; //bottom right
		if (board[width - 1][0] == player)
			count++; //top right
		return count;
	}
	
	/**
	 * @param player
	 *            the player number for which to get the number of pieces near, but not on, edges
	 * @return returns number of pieces near, but not on, corners that belong to <tt>player</tt>
	 */
	public int getNearCorners(int player) {
		int count = 0;
		if (board[0][1] == player)
			count++;
		if (board[1][0] == player)
			count++;
		if (board[1][1] == player)
			count++;
		if (board[0][height - 2] == player)
			count++;
		if (board[1][height - 1] == player)
			count++;
		if (board[1][height - 2] == player)
			count++;
		if (board[width - 1][height - 1] == player)
			count++;
		if (board[width - 2][height - 1] == player)
			count++;
		if (board[width - 1][height - 2] == player)
			count++;
		if (board[width - 2][height - 2] == player)
			count++;
		if (board[width - 2][0] == player)
			count++;
		if (board[width - 1][height - 2] == player)
			count++;
		if (board[width - 2][height - 2] == player)
			count++;
		return count;
	}
	
	/**
	 * @param player
	 *            the player number for which to get the number of pieces on top of columns
	 * @return number of pieces on top of columns belonging to <tt>player</tt>
	 */
	public int getOnTop(int player) {
		int count = 0;
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (board[i][j] == player) {
					count++;
					break;
				}
				else {
					continue;
				}
			}
		}
		return count;
	}
	
	/**
	 * @param player
	 *            the player number for which to get the number of pieces in the left half of the board
	 * @return number of pieces in the left half of the board belonging to <tt>player</tt>
	 */
	public int getLeftHalf(int player) {
		int count = 0;
		for (int i = 0; i < width / 2; i++) {
			for (int j = 0; j < height; j++) {
				if (board[i][j] == player)
					count++;
			}
		}
		return count;
	}
	
	/**
	 * @param player
	 *            the player number for which to get the number of pieces in the right half of the board
	 * @return number of pieces in the right half of the board belonging to <tt>player</tt>
	 */
	public int getRightHalf(int player) {
		int count = 0;
		for (int i = width / 2; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (board[i][j] == player)
					count++;
			}
		}
		return count;
	}
	
	/**
	 * Counts on the vertical average column, horizontal average column, and the longest diagonals
	 * 
	 * @param player
	 *            the player number for which to get the number of pieces in the middle of the board
	 * @return number of pieces in the middle of the board belonging to <tt>player</tt>
	 */
	public int getMiddle(int player) {
		int count = 0;
		int range = width / 3;
		for (int i = 0; i <= range; i++) {
			for (int j = 0; j < height; j++) {
				if (board[i][j] == player)
					count++;
			}
		}
		for (int x = range; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (board[x][y] == player)
					count++;
			}
		}
		return count;
	}
	
	/**
	 * @return {@link CNConfiguration#BoardWidth}
	 */
	public int constant() {
		return width;
	}
	
	/**
	 * @return a random int in [0, {@link CNConfiguration#BoardWidth})
	 */
	public int random() {
		return rgen.nextInt(width);
	}
	
	/**
	 * @return returns last column played
	 */
	public int lastMove() {
		return lastColumn;
	}
	
	/**
	 * Determines if the game is over after the given player's move.
	 * 
	 * @param lastPlayer
	 *            the last player to make a move in the current game.
	 * @return <tt>lastPlayer</tt> if the player won, 0 if there was a tie, and -1 if the game isn't over
	 */
	public int isGameOver(int lastPlayer) {
		return getMaxAdjacentPieces(lastPlayer) >= connectNum ? lastPlayer : (totalPieces == maxPieces ? 0 : -1);
	}
	
	/**
	 * @return true if the total number of pieces in the board is equal to the maximum number of pieces the board can hold
	 */
	public boolean isTie() {
		return totalPieces == maxPieces;
	}
	
	/**
	 * @return the width of the board
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * @return the height of the board
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Convenience method. Calls {@link #toString()} and prints the result.
	 * 
	 * @see #toString()
	 */
	public void printBoard() {
		System.out.println(toString());
	}
	
	/**
	 * Forwards to {@link #toString(int)} with a spacing of 1
	 */
	@Override
	public String toString() {
		return toString(1);
	}
	
	/**
	 * Produces a {@link String} representation of this {@link Board}.<br>
	 * The representation is in ASCII, and uses blanks to denote unfilled spots and the Player number to denote their pieces.
	 * 
	 * @param spacing
	 *            the spacing to place around piece numbers. Must be &ge; 0
	 * @return a {@link String} representation of this {@link Board}
	 */
	public String toString(int spacing) {
		if (spacing < 0)
			throw new IllegalArgumentException("Cannot have a spacing of less than 0.");
		/*int capacity = (spacing * 2 + pieceDisplayWidth) * (spacing * 2 + 1); //Number of characters in a well
		capacity += (spacing * 2 + 1); //Number of characters that form the left wall of a well
		capacity += (spacing * 2 + pieceDisplayWidth + 1); //Number of characters that form the bottom of a well
		int rowWidth = (spacing * 2 + pieceDisplayWidth + 1) * width + 2; //+1 for the remaining '|' or '+' +1 for the '\n'
		StringBuilder o = new StringBuilder(capacity);*/
		String out = "";
		int numDashes = spacing + pieceDisplayWidth + spacing;
		String space = "", numberMarker = "%-" + pieceDisplayWidth + "d";
		for (int i = 0; i < spacing; i++)
			space += " ";
		String horizontalNumber = space + numberMarker + space, horizontalZero = space;
		for (int i = 0; i < pieceDisplayWidth; i++)
			horizontalZero += " ";
		horizontalZero += space;
		String dashes = "";
		for (int i = 0; i < numDashes; i++)
			dashes += "-";
		String horizontalDashed = "+", horizontalSpaced = "|";
		for (int i = 0; i < width; i++) {
			horizontalDashed += dashes + "+";
			horizontalSpaced += horizontalZero + "|"; //Horizontal zero is the correct width for a piece well already, so we'll just reuse it here
		}
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < spacing; j++)
				out += "\n" + horizontalSpaced;
			out += "\n";
			for (int j = 0; j < width; j++)
				out += "|" + (board[i][j] == 0 ? horizontalZero : String.format(horizontalNumber, board[i][j]));
			out += "|";
			for (int j = 0; j < spacing; j++)
				out += "\n" + horizontalSpaced;
			out += "\n" + horizontalDashed;
		}
		return out.substring(1); //Remove the leading newline
	}
	
	/**
	 * Produces a {@link String} representation of this board via {@link #toString(int)} with the provided <tt>spacing</tt>.
	 * 
	 * @param spacing
	 *            the spacing to place around piece numbers. Must be &ge; 0
	 * @param substitutions
	 *            an array of (original, replacement) pairs where even indices are originals and odd indices are
	 *            replacements. <i>These are processed as regex</i>.
	 * @return a {@link String} representation of this {@link Board}
	 */
	public String toString(int spacing, String[] substitutions) {
		String out = toString(spacing);
		for (int i = 0; i < substitutions.length; i += 2)
			out.replaceAll(substitutions[i], substitutions[i + 1]);
		return out;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Board))
			return false;
		Board b = (Board) o;
		if (b.width != width || b.height != height || b.pieces.length != pieces.length)
			return false;
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				if (b.board[i][j] != board[i][j])
					return false;
		return true;
	}
	
	/**
	 * Calculates the fitnesses for a game of Connect N with any number of players.
	 * 
	 * @param winner
	 *            the player that won. A value less than 1 indicates a tie.
	 * @return the fitnesses of all of the participants in the game.
	 */
	public double[] calcFitnesses(int winner) {
		winner--; //Convert player number to array number
		double[] fitness = new double[pieces.length];
		double maxPlayed = ((double) maxPieces) / ((double) pieces.length);
		//If it is a tie, all of the players get the looser fitness (how long they lasted) 
		for (int i = 0; i < fitness.length; i++) {
			if (i == winner)
				fitness[i] = pieces[i] - connectNum + 1;
			else
				fitness[i] = maxPlayed - pieces[i];
		}
		return fitness;
	}
	
	public double maxPiecesPerPlayer() {
		return ((double) maxPieces) / ((double) pieces.length);
	}
}
