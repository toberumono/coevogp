package testHarnesses.connectn.game;

import coevogp.config.Clone;
import coevogp.config.Configuration;
import coevogp.config.Property;

public class CNConfiguration extends Configuration {
	@Property public int ConnectNum = 4;
	@Property public int BoardWidth = 7;
	@Property public int BoardHeight = 6;
	@Property public String[] Players = {"Evolved", "Random"};
	
	@Clone public Board board;
	
	public CNConfiguration() {
		board = new Board(this, Players.length);
	}
	
	public CNConfiguration(CNConfiguration source) {
		super(source);
	}
	
	@Override
	public synchronized Object clone() {
		return new CNConfiguration(this);
	}
}
