package testHarnesses.connectn.game;

import java.util.Arrays;
import java.util.function.Function;

import coevogp.Game;
import coevogp.gp.Gene;

public class CNGame implements Game<CNConfiguration, CNPlayer, Integer> {
	
	@Override
	public double[] play(CNConfiguration cfg, CNPlayer[] players) {
		if (players.length < cfg.NumPlayers) {
			double[] out = new double[players.length];
			for (int i = 0; i < players.length; i++)
				out[i] = playRandom(cfg, makePlayer(1, players[i].getMoveFunction()));
			return out;
		}
		int moveLimit = getMoveLimit(cfg);
		cfg.board.reset();
		int winner = 0;
		//Steps through the players list and lets each player make a move.  After each move, it checks to see if the game is over and whether that player won or there was a tie.
		for (int p = 0, moves = 0; (winner = cfg.board.isGameOver(p + 1)) == -1; p = (p + 1) % players.length, moves++) {
			players[p].makeMove(cfg);
			if (moves >= moveLimit) {
				double[] fitness = new double[players.length];
				Arrays.fill(fitness, cfg.board.maxPiecesPerPlayer());
				return fitness;
			}
		}
		return cfg.board.calcFitnesses(winner);
	}
	
	public int playVisual(CNConfiguration cfg, CNPlayer[] players) {
		cfg.board.reset();
		int p = 0, winner = 0;
		//Steps through the players list and lets each player make a move.  After each move, it checks to see if the game is over and whether that player won or there was a tie.
		for (; (winner = cfg.board.isGameOver(p + 1)) == -1; p = (p + 1) % players.length)
			players[p].makeMove(cfg);
		return winner;
	}
	
	@Override
	public CNPlayer makePlayer(int number, final Gene<CNConfiguration, Integer> gene) {
		return new CNPlayer(number, gene::evaluate);
	}
	
	@Override
	public CNPlayer makePlayer(int number, Function<CNConfiguration, Integer> move) {
		return new CNPlayer(number, move);
	}
	
	@Override
	public double playRandom(CNConfiguration cfg, CNPlayer player) {
		CNPlayer random = CNPlayer.makePlayer("random", 2, null);
		return play(cfg, new CNPlayer[]{player, random})[0];
	}
	
	@Override
	public int playRandomWon(CNConfiguration cfg, CNPlayer player) {
		CNPlayer random = CNPlayer.makePlayer("random", 2, null);
		play(cfg, new CNPlayer[]{player, random});
		return cfg.board.isGameOver(1) == 1 ? 1 : 0; //isGameOver returns -1 if the game times out
	}
	
	@Override
	public CNPlayer[] makeArray(int size) {
		return new CNPlayer[size];
	}
	
	public int getMoveLimit(CNConfiguration cfg) {
		return cfg.BoardWidth * cfg.BoardHeight * cfg.NumPlayers;
	}
}
