package testHarnesses.connectn.game;

import java.util.HashMap;
import java.util.function.BiFunction;
import java.util.function.Function;

import coevogp.Player;

import static coevogp.Helpers.rgen;

public class CNPlayer extends Player<CNConfiguration> {
	private final Function<CNConfiguration, Integer> move;
	private static final HashMap<String, BiFunction<Integer, Function<CNConfiguration, Integer>, CNPlayer>> constructors = new HashMap<>();
	static {
		constructors.put("random", (number, move) -> new CNPlayer(number, cfg -> rgen.nextInt(cfg.BoardWidth)));
		constructors.put("evolved", (number, move) -> new CNPlayer(number, move));
		constructors.put("human", (number, move) -> new CNPlayer(number, move));
	}
	
	/**
	 * Creates a new {@link CNPlayer} with the given player number
	 * 
	 * @param number
	 *            the id of this player, which is greater than 0
	 * @param move
	 *            the function that is used to determine which column [0, boardWidth) the player will place a piece in
	 */
	public CNPlayer(int number, Function<CNConfiguration, Integer> move) {
		super(number);
		this.move = move;
	}
	
	@Override
	public void makeMove(CNConfiguration cfg) {
		cfg.board.insertPiece(getNumber(), move.apply(cfg));
	}
	
	/**
	 * Constructs a new {@link CNPlayer} of the given type
	 * 
	 * @param type
	 *            the type of player (random, evolved, human) to construct
	 * @param number
	 *            the player number
	 * @param move
	 *            the function that is used to determine which column [0, boardWidth) in which the player will place a piece.
	 *            This parameter is ignored for random players.
	 * @return a {@link CNPlayer} constructed as specified
	 */
	public static final CNPlayer makePlayer(String type, int number, Function<CNConfiguration, Integer> move) {
		return constructors.get(type.toLowerCase()).apply(number, move);
	}

	public Function<CNConfiguration, Integer> getMoveFunction() {
		return move;
	}
}