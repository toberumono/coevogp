package testHarnesses.connectn.game;

import coevogp.GPRun;
import coevogp.gp.GP;
import coevogp.gp.NodeSet;
import coevogp.gp.Population;

public class ConnectN extends GPRun<CNConfiguration, GP<CNConfiguration, Integer>, CNPlayer, Integer> {
	private static final String numberFormatter = "%.2f";
	
	public ConnectN(String baseName) {
		super(baseName, true);
	}
	
	public CNConfiguration getCFG() {
		return this.cfg;
	}
	
	@Override
	protected NodeSet<CNConfiguration, Integer> createNodeSet(CNConfiguration cfg) {
		NodeSet<CNConfiguration, Integer> ns = new NodeSet<>(17);
		ns.addNode(1, 2, "+", (c, gene) -> ((gene.get(0).evaluate(c) + gene.get(1).evaluate(c))) % c.BoardWidth);
		ns.addNode(2, 2, "-", (c, gene) -> {
			int a = gene.get(0).evaluate(c) - gene.get(1).evaluate(c), b = c.BoardWidth;
			return (a % b + b) % b;
		});
		ns.addNode(3, 2, "*", (c, gene) -> ((gene.get(0).evaluate(c) * gene.get(1).evaluate(c))) % c.BoardWidth);
		ns.addNode(4, 2, "%", (c, gene) -> {
			int val = gene.get(1).evaluate(c);
			if (val == 0)
				return (gene.get(0).evaluate(c) / (val + 1)) % c.BoardWidth;
			else
				return (gene.get(0).evaluate(c) / val) % c.BoardWidth;
		});
		ns.addNode(5, 0, "red", (c, gene) -> c.board.getNumPieces(1));
		ns.addNode(6, 0, "black", (c, gene) -> c.board.getNumPieces(2));
		ns.addNode(7, 0, "adjR", (c, gene) -> c.board.getMaxAdjacentPieces(1));
		ns.addNode(8, 0, "adjB", (c, gene) -> c.board.getMaxAdjacentPieces(2));
		ns.addNode(9, 0, "edgeR", (c, gene) -> c.board.getEdges(1));
		ns.addNode(10, 0, "edgeB", (c, gene) -> c.board.getEdges(2));
		ns.addNode(11, 0, "nEdgeR", (c, gene) -> c.board.getNearEdges(1));
		ns.addNode(12, 0, "nEdgeB", (c, gene) -> c.board.getNearEdges(2));
		ns.addNode(13, 0, "cornR", (c, gene) -> c.board.getCorners(1));
		ns.addNode(14, 0, "cornB", (c, gene) -> c.board.getCorners(2));
		ns.addNode(15, 0, "nCornR", (c, gene) -> c.board.getNearCorners(1));
		ns.addNode(16, 0, "nCornB", (c, gene) -> c.board.getNearCorners(2));
		ns.addNode(17, 0, "C", (c, gene) -> c.board.constant());
		
		// ORIGINAL NODES (the ones we wrote)
		// ns.addNode(18, 2, "min", (c, gene) -> {
		// 	int v1 = gene.get(0).evaluate(c), v2 = gene.get(1).evaluate(c);
		// 	return v1 < v2 ? v1 : v2;
		// });
		// ns.addNode(19, 2, "max", (c, gene) -> {
		// 	int v1 = gene.get(0).evaluate(c), v2 = gene.get(1).evaluate(c);
		// 	return v1 > v2 ? v1 : v2;
		// });
		// ns.addNode(20, 0, "rand", (c, gene) -> c.board.random());
		// ns.addNode(21, 0, "topR", (c, gene) -> c.board.getOnTop(1));
		// ns.addNode(22, 0, "topB", (c, gene) -> c.board.getOnTop(2));
		// ns.addNode(23, 0, "rightR", (c, gene) -> c.board.getRightHalf(1));
		// ns.addNode(24, 0, "rightB", (c, gene) -> c.board.getRightHalf(2));
		// ns.addNode(25, 0, "leftR", (c, gene) -> c.board.getLeftHalf(1));
		// ns.addNode(26, 0, "leftB", (c, gene) -> c.board.getLeftHalf(2));
		// ns.addNode(27, 0, "midR", (c, gene) -> c.board.getMiddle(1));
		// ns.addNode(28, 0, "midB", (c, gene) -> c.board.getMiddle(2));
		// ns.addNode(29, 0, "last", (c, gene) -> c.board.lastMove());
		// ns.addNode(30, 0, "next", (c, gene) -> c.board.nextToLast());
		return ns;
	}
	
	@Override
	protected Population<CNConfiguration, CNPlayer, Integer> createPopulation(CNConfiguration cfg, NodeSet<CNConfiguration, Integer> ns) {
		return new Population<>(cfg, ns, new CNGame());
	}
	
	@Override
	protected CNConfiguration createConfiguration() {
		return new CNConfiguration();
	}
	
	public static void main(String[] args) {
		//compute base file name from command line parameter
		String baseName;
		if (args.length == 1)
			baseName = args[0];
		else
			baseName = "ConnectN";
		
		//construct the test case
		ConnectN test = new ConnectN(baseName);
		
		//run the test
		test.run();
		
		CNPlayer p1 = CNPlayer.makePlayer("evolved", 1, test.getBest().get(0)::evaluate);
		CNPlayer p2 = CNPlayer.makePlayer("random", 2, null);
		
		CNGame game = new CNGame();
		double wins = 0.0;
		for (int i = 0; i < 500; i++) {
			int won = game.playVisual(test.getCFG(), new CNPlayer[]{p1, p2});
			wins += won == 1 ? 1 : 0;
		}
		System.out.println("Win rate: " + String.format(numberFormatter, wins / 500.0 * 100) + "%");
		
		//make sure all threads are killed
		System.exit(0);
	}
	
}
