package testHarnesses.connectn.game;

@FunctionalInterface
public interface GUIHook {
	public void update(int column, int row, int player);
}
