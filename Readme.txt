Sections:

I:   People in Our Group
II:  External Code
III: Modifications
IV:  How to Run

I: People in Our Group
	Joyce Yu, Jake Highleyman, Marco Dow, Joshua Lipstone

II: External Code
	Nothing beyond what was already in gpjpp (the Acme package)

III: Modifications
	The new code files all contain descriptions of how they are used and, when possible, what they do at the top.
	Also, the ordering here doesn't really mean anything - the numbers just help identify section breaks.
	Last, switching to generics naturally required changes to all of the classes; however, those changes are not noted unless they are related to a significant functionality change.
	1) The coevogp package
		Container is functionally the same - it just has a few new convenience methods.
		GPRun is an awkward fusion of old and new code.  This one is the least-elegantly changed; however, none of the changes are particularly important relative to those in other files.
		Game and Player are used so that custom co-evolution algorithms can be more easily implemented.
		RandomCoin replaces GPRandom and adds a nextInt method that takes a range.
	2) The coevogp.config package
		The config package is, while loosely based on gpjpp's original system, the only parts remaining that are from gpjpp are the names of the variables and a few of the methods.
		Configuration replaced GPVariables, and uses the Java Reflections API to dynamically generate the copy constructor and the load, equals, and printOn methods for all of its subclasses at runtime.
		The String[]-based enums from the original library have been replaced with actual enums and make further use of Reflections to dynamically load them from Strings.
		See the documentation in Configuration, Properties, Property, Copy, and Clone files in that package for a full explanation of how to use the new system.
	3) The coevogp.gp package
		The gp package contains a decent amount of code from the original gpjpp library; however, most of it has been heavily modified.
		Drawing, GenePrint, and GeneReference are effectively the same.
		The Connector, Operator, and OperatorConstructor classes form the system that allows for the parallelization used in our evolution algorithm.
		The Node class now holds the id, string representation, number of arguments required, and the action that is performed by a Gene with that node type as a lambda defined by NodeAction.
		NodeSet is mostly the same as the original, but some of the methods have been updated to work with the changes in Node and/or to make things a bit more convenient.
		Population is an awkward mix of original gpjpp code and new code.  The Mutator and Tournament nested classes are new, as is the logic in generate; however the rest is mostly the same.
	4) The game package
		CNConfiguration, CNGame, and CNPlayer, Board, and ConnectN are used to implement the GA code specific to our project.
		GUIHook and GUIHook2 work with the GUI.
	5) The graphicsTest package
		This package contains the undeniably high-end GUI for playing a Connect Four game.

IV: How to Run
	The project includes an ant buildfile (build.xml).  Use ant to run it.  If you don't have Eclipse (which includes ant), installing ant via homebrew is far easier and better than using ant's original installer.  Make sure that you have javac version 1.8.0_45 or later - there is a bug in earlier versions of the compiler that causes problems when it tries to compile some code involving Streams.
	The buildfile produces ConnectN.jar and ConnectNVisual.jar.  Use "java -jar ConnectN.jar" to run the main algorithm.  Make sure that there is a "data" folder first.
	All of the variables can be changed from ConnectN.ini which is auto-generated the first time the program is run.
	Do NOT set goodRuns to be greater than 0 - the program will loop infinitely.